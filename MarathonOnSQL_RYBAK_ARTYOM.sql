
USE master
GO

IF NOT EXISTS(select * from sys.databases where name='Training_RYBAK')
CREATE DATABASE Training_RYBAK
GO

USE Training_RYBAK
GO

IF OBJECT_ID('dbo.cashFlowView') IS NOT NULL
DROP VIEW dbo.cashFlowView
GO

IF OBJECT_ID ( 'dbo.CreateTables', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.CreateTables;  
GO

IF OBJECT_ID ( 'dbo.InsertExcelDataTables', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.InsertExcelDataTables;  
GO

IF OBJECT_ID ( 'dbo.InsertInfoFromExcelTable', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.InsertInfoFromExcelTable;  
GO

IF OBJECT_ID ( 'dbo.GenerateTransactions', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.GenerateTransactions;  
GO

IF OBJECT_ID ( 'dbo.DenominationTransactions', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.DenominationTransactions;  
GO

IF OBJECT_ID ( 'dbo.ExecuteTransactions', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.ExecuteTransactions;  
GO

IF OBJECT_ID('ExecuteTransfer', 'P') IS NOT NULL
DROP PROCEDURE ExecuteTransfer
GO

IF OBJECT_ID('CheckAndExecuteTransfers', 'P') IS NOT NULL
DROP PROCEDURE CheckAndExecuteTransfers
GO

IF OBJECT_ID('ExecuteBorrowFromFriends', 'P') IS NOT NULL
DROP PROCEDURE ExecuteBorrowFromFriends
GO

IF OBJECT_ID('dbo.rndView', 'V') IS NOT NULL
DROP VIEW dbo.rndView
GO


IF OBJECT_ID('dbo.cashFlowView', 'V') IS NOT NULL
DROP VIEW dbo.cashFlowView
GO

IF OBJECT_ID('dbo.generateRandomNumber') IS NOT NULL
  DROP FUNCTION dbo.generateRandomNumber
GO

IF OBJECT_ID('dbo.generateTableRandomNumbers') IS NOT NULL
  DROP FUNCTION dbo.generateTableRandomNumbers
GO

IF OBJECT_ID('dbo.getDateTable') IS NOT NULL
DROP FUNCTION dbo.getDateTable
GO


IF OBJECT_ID('dbo.getAmountAccount') IS NOT NULL
DROP FUNCTION dbo.getAmountAccount
GO

IF OBJECT_ID('dbo.getAmountAccounts') IS NOT NULL
DROP FUNCTION dbo.getAmountAccounts
GO

IF OBJECT_ID('Rates') IS NOT NULL
  DROP TABLE Rates
GO

IF OBJECT_ID('UserTransactions') IS NOT NULL
  DROP TABLE UserTransactions
GO

IF OBJECT_ID('Denominations') IS NOT NULL
  DROP TABLE Denominations
GO

IF OBJECT_ID('CrossCourses') IS NOT NULL
  DROP TABLE CrossCourses
GO

IF OBJECT_ID('Transactions') IS NOT NULL
  DROP TABLE Transactions
GO

IF OBJECT_ID('TypesOfTransaction') IS NOT NULL
  DROP TABLE TypesOfTransaction
GO

IF OBJECT_ID('CategoriesOfTransaction') IS NOT NULL
  DROP TABLE CategoriesOfTransaction
GO

IF OBJECT_ID('Accounts') IS NOT NULL
  DROP TABLE Accounts
GO

IF OBJECT_ID('TypesOfMoney') IS NOT NULL
  DROP TABLE TypesOfMoney
GO

IF OBJECT_ID('dbo.Split') IS NOT NULL
DROP FUNCTION dbo.[Split]
GO

IF OBJECT_ID('checkTypeName') IS NOT NULL
DROP FUNCTION checkTypeName
GO

CREATE FUNCTION [dbo].[Split]
(
    @String NVARCHAR(4000),
    @Delimiter NCHAR(1)
)
RETURNS TABLE
AS
RETURN
(
    WITH Split(stpos,endpos)
    AS(
        SELECT 0 AS stpos, CHARINDEX(@Delimiter,@String) AS endpos
        UNION ALL
        SELECT endpos+1, CHARINDEX(@Delimiter,@String,endpos+1)
            FROM Split
            WHERE endpos > 0
    )
    SELECT 'Id' = ROW_NUMBER() OVER (ORDER BY (SELECT 1)),
        'Data' = SUBSTRING(@String,stpos,COALESCE(NULLIF(endpos,0),LEN(@String)+1)-stpos)
    FROM Split
)
GO

CREATE VIEW rndView
AS
SELECT RAND() rndResult
GO

CREATE FUNCTION generateRandomNumber(@a int, @b int)
RETURNS int
BEGIN
	DECLARE @result int
	IF (@b > @a)
	BEGIN
		SET @result = CAST(@a + (@b - @a)*(SELECT* FROM rndView) AS int)
	END
	ELSE
	BEGIN
		SET @result = CAST(@b + (@a - @b)*(SELECT* FROM rndView) AS int)
	END
	RETURN @result
END
GO

CREATE FUNCTION generateTableRandomNumbers(@rowCount int, @a int, @b int)
RETURNS @resultTable TABLE ( number int NOT NULL )
BEGIN
	DECLARE @counter int
	SET @counter = 0
	WHILE @counter < @rowCount
	BEGIN
		INSERT INTO @resultTable
		(number)
		VALUES(dbo.generateRandomNumber(@a, @b))
		SET @counter = @counter + 1
	END
	RETURN
END
GO

--CREATE FUNCTION getDateTable(@startDate datetime, @endDate datetime)
--RETURNS @resultTable TABLE ( [date] datetime NOT NULL )
--BEGIN
--	DECLARE @counter datetime
--	SET @counter = @startDate
--	WHILE @counter <= @endDate
--	BEGIN
--		INSERT INTO @resultTable
--		([date])
--		VALUES(@counter)
--		SET @counter = DATEADD(day, 1, @counter)
--	END
--	RETURN
--END
--GO

CREATE FUNCTION getDateTable()
RETURNS @resultTable TABLE ( [Date] datetime NOT NULL )
BEGIN
	DECLARE @counter datetime
	DECLARE @endDate datetime
	SET @counter = (SELECT TOP 1 CourseDate FROM CrossCourses ORDER BY CourseDate)
	SET @endDate = (SELECT TOP 1 CourseDate FROM CrossCourses ORDER BY CourseDate DESC)
	WHILE @counter <= @endDate
	BEGIN
		INSERT INTO @resultTable
		([Date])
		VALUES(@counter)
		SET @counter = DATEADD(day, 1, @counter)
	END
	RETURN
END
GO

CREATE FUNCTION getAmountAccount(@AId int)
RETURNS @resultTable TABLE 
( 
	Amount money NOT NULL,
	[Date] datetime NOT NULL
)
BEGIN
	DECLARE @result money
	DECLARE @incRes money
	DECLARE @excRes money
	DECLARE @counterDate datetime
	DECLARE @endDate datetime
	SET @counterDate = (SELECT TOP 1 CC.CourseDate FROM CrossCourses CC ORDER BY CC.CourseDate)
	SET @endDate = (SELECT TOP 1 CC.CourseDate FROM CrossCourses CC ORDER BY CC.CourseDate DESC)
	SET @result = 0
	SET @incRes = 0
	SET @excRes = 0
	WHILE @counterDate <= @endDate
	BEGIN
		SET @incRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @AId) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Inc'
		GROUP BY CT.Name)

		SET @excRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @AId) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Exp'
		GROUP BY CT.Name)

		IF @incRes IS NULL
		BEGIN
			SET @incRes = 0
		END
		
		IF @excRes IS NULL
		BEGIN
			SET @excRes = 0
		END

		SET @result = @result +  @incRes - @excRes
		INSERT INTO @resultTable
		(Amount, [Date])
		VALUES(@result, @counterDate)

		SET @counterDate = DATEADD(day, 1, @counterDate)
		SET @incRes = 0
		SET @excRes = 0
	END
	RETURN 
END
GO

CREATE FUNCTION getAmountAccounts()
RETURNS @resultTable TABLE 
( 
	AmountBr money NOT NULL,
	AmountUsd money NOT NULL,
	[Date] datetime NOT NULL
)
BEGIN
	DECLARE @resultBr money
	DECLARE @resultUsd money
	DECLARE @incRes money
	DECLARE @excRes money
	DECLARE @counterDate datetime
	DECLARE @endDate datetime
	DECLARE @accountBr int
	DECLARE @accountUsd int
	SET @accountBr = (SELECT A.Id FROM Accounts A
						INNER JOIN TypesOfMoney TP ON TP.Id = A.TypeOfMoneyId
						WHERE TP.Name = 'Br')
	SET @accountUsd = (SELECT A.Id FROM Accounts A
						INNER JOIN TypesOfMoney TP ON TP.Id = A.TypeOfMoneyId
						WHERE TP.Name = 'Usd')
	SET @counterDate = (SELECT TOP 1 CC.CourseDate FROM CrossCourses CC ORDER BY CC.CourseDate)
	SET @endDate = (SELECT TOP 1 CC.CourseDate FROM CrossCourses CC ORDER BY CC.CourseDate DESC)
	SET @resultBr = 0
	SET @resultUsd = 0
	SET @incRes = 0
	SET @excRes = 0
	WHILE @counterDate <= @endDate
	BEGIN
		SET @incRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountBr) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Inc'
		GROUP BY CT.Name)

		SET @excRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountBr) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Exp'
		GROUP BY CT.Name)

		IF @incRes IS NULL
		BEGIN
			SET @incRes = 0
		END
		
		IF @excRes IS NULL
		BEGIN
			SET @excRes = 0
		END

		SET @resultBr = @resultBr +  @incRes - @excRes

		SET @incRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountUsd) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Inc'
		GROUP BY CT.Name)

		SET @excRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountUsd) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Exp'
		GROUP BY CT.Name)

		IF @incRes IS NULL
		BEGIN
			SET @incRes = 0
		END
		
		IF @excRes IS NULL
		BEGIN
			SET @excRes = 0
		END

		SET @resultUsd = @resultUsd +  @incRes - @excRes
		
		
		INSERT INTO @resultTable
		(AmountBr, AmountUsd, [Date])
		VALUES(@resultBr, @resultUsd, @counterDate)

		SET @counterDate = DATEADD(day, 1, @counterDate)
		SET @incRes = 0
		SET @excRes = 0
	END
	RETURN 
END
GO

CREATE PROCEDURE CreateTables
AS

CREATE TABLE TypesOfMoney
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
)

CREATE TABLE Denominations
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	DifferenceToOne money NOT NULL,
	TypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id)
)

CREATE TABLE CrossCourses
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	FirstTypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	SecondTypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	[Difference] float NOT NULL,
	CourseDate datetime NOT NULL default(GETDATE())
)

CREATE TABLE Accounts
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Total money NOT NULL default(0),
	TypeOfMoneyId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	CreateDate datetime NOT NULL default(GETDATE()),
	DeleteDate datetime NULL
)

CREATE TABLE CategoriesOfTransaction
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name varchar(250) NOT NULL
)

CREATE TABLE TypesOfTransaction
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name varchar(250) NOT NULL,
	CategoryId int NOT NULL FOREIGN KEY REFERENCES CategoriesOfTransaction(Id)
)

CREATE TABLE Transactions
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Amount money NOT NULL default(0),
	TypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfTransaction(Id),
	AccountId int NOT NULL FOREIGN KEY REFERENCES Accounts(Id),
	Note varchar(250) NOT NULL default(''),
	CreateDate datetime NOT NULL default(GETDATE()),
	DeleteDate datetime NULL
)

CREATE TABLE Rates(
	RateDate [datetime] NULL,
	Total [nvarchar](255) NULL
) ON [PRIMARY]

CREATE TABLE UserTransactions(
	[Type] [nvarchar](255) NULL,
	[Operation Name] [nvarchar](255) NULL,
	[AmountMin] [float] NULL,
	[AmountMax] [float] NULL,
	[Currency] [nvarchar](255) NULL,
	[Rate] [float] NULL,
	[Period] [nvarchar](255) NULL,
	[Account] [nvarchar](255) NULL
) ON [PRIMARY]
GO

CREATE PROCEDURE InsertExcelDataTables
AS
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-01T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-02T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-03T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-04T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-05T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-06T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-07T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-08T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-09T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-12T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-13T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-15T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-16T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-18T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-19T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-20T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-21T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-22T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-23T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-24T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-25T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-26T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-27T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-28T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-29T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-30T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-01-31T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-01T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-02T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-03T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-04T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-05T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-06T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-07T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-08T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-09T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-10T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-11T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-12T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-13T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-14T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-15T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-16T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-17T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-19T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-20T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-22T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-23T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-24T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-25T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-26T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-27T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-02-28T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-01T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-02T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-03T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-04T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-05T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-06T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-07T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-08T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-09T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-10T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-11T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-12T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-13T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-14T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-15T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-16T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-18T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-19T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-20T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-23T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-26T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-27T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-29T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-30T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-03-31T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-01T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-02T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-03T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-04T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-05T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-06T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-07T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-08T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-09T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-10T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-11T00:00:00.000' AS DateTime), N'15 450')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-12T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-13T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-14T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-15T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-16T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-17T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-18T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-19T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-20T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-21T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-22T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-23T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-24T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-25T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-26T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-27T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-28T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-04-30T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-01T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-02T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-03T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-04T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-05T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-06T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-07T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-08T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-09T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-10T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-11T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-12T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-13T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-14T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-15T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-16T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-17T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-18T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-19T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-20T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-21T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-22T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-23T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-24T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-26T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-27T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-28T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-29T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-30T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-05-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-04T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-05T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-06T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-08T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-09T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-10T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-11T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-12T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-13T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-14T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-15T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-16T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-17T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-18T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-19T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-20T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-21T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-22T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-23T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-24T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-25T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-26T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-27T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-28T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-29T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-06-30T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-01T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-02T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-03T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-04T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-09T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-10T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-11T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-12T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-13T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-14T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-15T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-16T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-17T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-18T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-19T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-20T00:00:00.000' AS DateTime), N'15 270')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-21T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-22T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-23T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-24T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-25T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-26T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-27T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-28T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-29T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-30T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-07-31T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-01T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-02T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-03T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-04T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-05T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-06T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-07T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-09T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-10T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-12T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-13T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-14T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-15T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-16T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-17T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-18T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-19T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-20T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-21T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-22T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-23T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-24T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-25T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-26T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-27T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-28T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-29T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-30T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-08-31T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-01T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-02T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-03T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-04T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-05T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-06T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-07T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-08T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-09T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-10T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-11T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-12T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-13T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-15T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-16T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-17T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-18T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-19T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-20T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-21T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-22T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-23T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-24T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-25T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-26T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-27T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-28T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-29T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-09-30T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-01T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-02T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-03T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-04T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-05T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-06T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-07T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-08T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-09T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-10T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-11T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-12T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-13T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-14T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-15T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-16T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-17T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-19T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-20T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-21T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-22T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-23T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-24T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-25T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-26T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-27T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-28T00:00:00.000' AS DateTime), N'14 890')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-29T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-30T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-10-31T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-01T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-02T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-03T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-04T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-08T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-09T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-11T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-12T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-13T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-14T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-15T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-16T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-17T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-18T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-19T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-20T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-21T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-22T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-23T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-24T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-25T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-26T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-27T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-28T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-29T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-11-30T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-01T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-02T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-03T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-04T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-05T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-06T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-07T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-08T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-09T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-10T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-12T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-13T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-14T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-15T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-16T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-17T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-18T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-19T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-20T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-21T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-22T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-23T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-24T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-25T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-26T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-27T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-28T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-29T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-30T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2010-12-31T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-01T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-02T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-03T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-04T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-05T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-06T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-07T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-08T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-09T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-10T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-11T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-12T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-13T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-15T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-16T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-17T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-18T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-19T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-20T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-21T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-22T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-23T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-24T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-26T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-27T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-28T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-29T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-30T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-01-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-04T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-05T00:00:00.000' AS DateTime), N'14 910')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-06T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-08T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-09T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-10T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-11T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-12T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-13T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-14T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-15T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-16T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-17T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-18T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-19T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-20T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-21T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-22T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-23T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-24T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-25T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-26T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-27T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-02-28T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-01T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-02T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-03T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-04T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-05T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-06T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-09T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-10T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-11T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-12T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-13T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-14T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-15T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-16T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-17T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-18T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-19T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-20T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-21T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-22T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-23T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-24T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-25T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-26T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-27T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-28T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-29T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-30T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-03-31T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-01T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-02T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-03T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-04T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-05T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-06T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-07T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-08T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-09T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-12T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-13T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-15T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-16T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-18T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-19T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-20T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-21T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-22T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-23T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-24T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-25T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-26T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-27T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-04-30T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-03T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-04T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-05T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-06T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-08T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-09T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-10T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-11T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-12T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-13T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-14T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-15T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-16T00:00:00.000' AS DateTime), N'15 410')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-17T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-18T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-19T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-20T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-21T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-22T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-23T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-24T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-25T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-26T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-27T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-28T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-29T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-30T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-05-31T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-01T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-02T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-04T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-07T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-08T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-09T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-10T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-11T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-12T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-13T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-14T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-15T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-16T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-17T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-18T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-19T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-20T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-21T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-22T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-23T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-24T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-25T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-26T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-27T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-28T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-29T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-06-30T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-01T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-02T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-03T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-04T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-05T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-06T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-07T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-09T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-10T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-12T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-13T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-14T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-15T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-16T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-17T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-18T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-19T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-20T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-21T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-22T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-23T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-24T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-25T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-26T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-27T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-28T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-30T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-07-31T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-01T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-02T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-03T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-04T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-05T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-06T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-07T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-08T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-09T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-10T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-11T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-12T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-13T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-14T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-15T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-16T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-17T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-18T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-19T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-20T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-21T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-22T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-23T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-24T00:00:00.000' AS DateTime), N'14 890')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-26T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-27T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-28T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-29T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-30T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-08-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-03T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-04T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-05T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-06T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-08T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-09T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-10T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-11T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-12T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-13T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-14T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-15T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-16T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-17T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-18T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-19T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-20T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-21T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-22T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-23T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-24T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-25T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-26T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-27T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-28T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-29T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-09-30T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-01T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-02T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-03T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-04T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-08T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-09T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-11T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-12T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-13T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-14T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-15T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-16T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-18T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-19T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-20T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-23T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-26T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-27T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-29T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-30T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-10-31T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-01T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-02T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-03T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-04T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-05T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-06T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-07T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-08T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-09T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-10T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-11T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-12T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-13T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-14T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-15T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-16T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-17T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-18T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-19T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-20T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-21T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-22T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-23T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-24T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-25T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-26T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-27T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-28T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-11-30T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-01T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-02T00:00:00.000' AS DateTime), N'14 860')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-03T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-04T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-05T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-06T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-07T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-08T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-09T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-10T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-11T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-12T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-13T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-14T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-15T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-16T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-17T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-18T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-19T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-20T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-21T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-22T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-23T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-24T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-26T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-27T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-28T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-29T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-30T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2011-12-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-04T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-05T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-06T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-08T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-09T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-10T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-11T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-12T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-13T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-14T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-15T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-16T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-19T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-20T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-21T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-22T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-23T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-24T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-25T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-26T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-27T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-28T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-29T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-30T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-01-31T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-01T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-02T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-03T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-04T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-05T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-06T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-07T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-08T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-09T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-10T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-11T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-12T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-13T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-14T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-15T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-16T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-18T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-19T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-20T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-23T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-26T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-27T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-02-29T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-01T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-02T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-03T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-04T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-05T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-06T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-07T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-08T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-09T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-10T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-11T00:00:00.000' AS DateTime), N'15 150')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-12T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-13T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-14T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-15T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-16T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-17T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-18T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-19T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-20T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-21T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-22T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-23T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-24T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-25T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-26T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-27T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-28T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-30T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-03-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-01T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-02T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-03T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-04T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-05T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-06T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-07T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-08T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-09T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-10T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-11T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-12T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-13T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-14T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-15T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-16T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-17T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-18T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-19T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-20T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-21T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-22T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-23T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-24T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-26T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-27T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-28T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-29T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-04-30T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-01T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-04T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-06T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-07T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-08T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-09T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-10T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-11T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-12T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-13T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-14T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-15T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-16T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-17T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-18T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-19T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-20T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-21T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-22T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-23T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-24T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-25T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-26T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-27T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-28T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-29T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-30T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-05-31T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-01T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-02T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-03T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-04T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-05T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-09T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-10T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-11T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-12T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-13T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-14T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-15T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-16T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-17T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-18T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-19T00:00:00.000' AS DateTime), N'15 210')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-20T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-21T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-22T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-23T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-24T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-25T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-26T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-27T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-28T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-29T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-06-30T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-01T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-02T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-03T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-04T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-05T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-06T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-07T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-08T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-09T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-12T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-13T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-15T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-16T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-18T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-19T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-20T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-21T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-22T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-23T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-24T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-25T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-26T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-27T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-28T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-29T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-30T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-07-31T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-01T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-02T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-03T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-04T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-05T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-06T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-07T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-08T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-09T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-10T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-11T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-12T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-13T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-14T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-15T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-16T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-17T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-19T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-20T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-22T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-23T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-24T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-25T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-26T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-27T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-28T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-29T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-30T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-08-31T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-01T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-02T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-03T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-04T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-05T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-06T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-07T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-08T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-09T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-10T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-11T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-12T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-13T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-14T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-15T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-16T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-17T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-18T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-19T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-20T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-21T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-23T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-24T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-25T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-26T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-27T00:00:00.000' AS DateTime), N'15 090')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-28T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-29T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-09-30T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-01T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-02T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-03T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-04T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-05T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-06T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-07T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-08T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-09T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-10T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-11T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-12T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-13T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-14T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-15T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-16T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-18T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-19T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-20T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-21T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-22T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-23T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-26T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-27T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-28T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-29T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-30T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-10-31T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-01T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-02T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-03T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-04T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-05T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-06T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-07T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-08T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-09T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-10T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-11T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-12T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-13T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-14T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-15T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-16T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-17T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-18T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-19T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-20T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-21T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-22T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-23T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-24T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-25T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-26T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-27T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-28T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-11-30T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-01T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-02T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-03T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-04T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-05T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-06T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-07T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-08T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-09T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-10T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-11T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-12T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-13T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-14T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-15T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-16T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-17T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-18T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-19T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-20T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-21T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-22T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-23T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-24T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-26T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-27T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-28T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-29T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-30T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2012-12-31T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-04T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-05T00:00:00.000' AS DateTime), N'14 910')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-06T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-07T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-08T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-09T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-10T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-11T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-12T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-13T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-14T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-15T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-16T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-17T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-18T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-19T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-20T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-21T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-22T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-23T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-24T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-25T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-26T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-27T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-28T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-29T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-30T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-01-31T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-01T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-02T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-03T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-04T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-09T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-10T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-11T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-12T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-13T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-14T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-15T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-16T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-18T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-19T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-20T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-21T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-22T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-23T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-26T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-27T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-02-28T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-01T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-02T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-03T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-04T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-05T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-06T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-07T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-08T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-09T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-10T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-11T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-12T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-13T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-14T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-15T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-16T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-17T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-18T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-19T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-20T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-21T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-22T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-23T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-24T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-26T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-27T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-28T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-29T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-30T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-03-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-03T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-04T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-05T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-06T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-08T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-09T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-10T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-11T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-12T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-13T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-14T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-15T00:00:00.000' AS DateTime), N'15 270')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-16T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-17T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-18T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-19T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-20T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-21T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-22T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-23T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-24T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-25T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-26T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-27T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-28T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-29T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-04-30T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-01T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-02T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-03T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-04T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-08T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-09T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-11T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-12T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-13T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-14T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-15T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-16T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-18T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-19T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-20T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-23T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-26T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-27T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-29T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-30T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-05-31T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-01T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-02T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-03T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-04T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-05T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-06T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-07T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-08T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-09T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-10T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-11T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-12T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-13T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-14T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-15T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-16T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-17T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-18T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-19T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-20T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-21T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-22T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-23T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-24T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-25T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-26T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-27T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-28T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-06-30T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-01T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-02T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-03T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-04T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-05T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-06T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-07T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-08T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-09T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-10T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-11T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-12T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-13T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-14T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-15T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-16T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-17T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-18T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-19T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-20T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-21T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-22T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-23T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-24T00:00:00.000' AS DateTime), N'14 890')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-25T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-26T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-27T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-28T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-29T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-30T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-07-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-02T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-04T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-05T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-06T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-08T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-09T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-10T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-11T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-12T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-13T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-14T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-15T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-16T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-19T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-20T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-21T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-22T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-23T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-24T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-25T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-26T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-27T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-28T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-29T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-30T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-08-31T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-01T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-02T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-03T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-04T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-05T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-06T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-07T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-08T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-09T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-10T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-11T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-12T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-13T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-14T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-15T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-16T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-18T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-19T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-20T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-23T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-26T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-27T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-29T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-09-30T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-01T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-02T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-03T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-04T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-05T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-06T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-07T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-08T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-09T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-10T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-11T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-12T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-13T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-14T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-15T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-16T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-17T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-18T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-19T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-20T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-21T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-22T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-23T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-24T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-25T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-26T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-27T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-28T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-30T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-10-31T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-01T00:00:00.000' AS DateTime), N'14 910')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-02T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-03T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-04T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-05T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-06T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-07T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-08T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-09T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-10T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-11T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-12T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-13T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-15T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-16T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-17T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-18T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-19T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-20T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-21T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-22T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-23T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-24T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-25T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-26T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-27T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-28T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-29T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-11-30T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-01T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-02T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-03T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-04T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-05T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-06T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-07T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-08T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-09T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-10T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-11T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-12T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-13T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-15T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-16T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-17T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-18T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-19T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-20T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-21T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-22T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-23T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-24T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-25T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-26T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-27T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-28T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-29T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-30T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2013-12-31T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-01T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-02T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-03T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-04T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-05T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-06T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-07T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-08T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-09T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-10T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-11T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-12T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-13T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-14T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-15T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-16T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-19T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-20T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-21T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-22T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-23T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-24T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-25T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-26T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-27T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-28T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-29T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-30T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-01-31T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-01T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-02T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-03T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-04T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-05T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-06T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-07T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-08T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-09T00:00:00.000' AS DateTime), N'15 400')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-10T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-11T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-12T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-13T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-14T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-15T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-16T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-17T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-18T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-19T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-20T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-23T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-24T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-25T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-26T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-27T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-02-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-01T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-02T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-03T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-04T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-05T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-06T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-07T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-08T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-09T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-10T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-11T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-12T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-13T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-14T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-15T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-16T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-17T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-18T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-19T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-20T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-21T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-22T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-23T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-24T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-25T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-26T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-27T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-28T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-29T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-30T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-03-31T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-01T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-02T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-03T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-04T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-05T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-06T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-07T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-08T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-09T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-10T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-11T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-12T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-13T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-14T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-15T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-16T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-17T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-18T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-19T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-20T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-21T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-22T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-23T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-24T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-25T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-26T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-27T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-28T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-29T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-04-30T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-01T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-02T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-03T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-04T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-05T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-06T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-07T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-08T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-09T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-10T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-11T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-12T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-13T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-14T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-15T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-16T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-17T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-18T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-19T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-20T00:00:00.000' AS DateTime), N'15 410')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-21T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-22T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-23T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-24T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-25T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-26T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-27T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-28T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-29T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-30T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-05-31T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-01T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-02T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-03T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-04T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-05T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-06T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-09T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-10T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-11T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-12T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-13T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-14T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-15T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-16T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-17T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-18T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-19T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-20T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-21T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-22T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-23T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-24T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-25T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-26T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-27T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-28T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-29T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-06-30T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-01T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-02T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-03T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-04T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-05T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-06T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-07T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-08T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-09T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-12T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-13T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-15T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-16T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-18T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-19T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-20T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-21T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-22T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-23T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-24T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-25T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-26T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-27T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-28T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-29T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-30T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-07-31T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-01T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-02T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-03T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-04T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-05T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-06T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-07T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-08T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-09T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-10T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-11T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-12T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-13T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-14T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-15T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-16T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-17T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-19T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-20T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-21T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-22T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-23T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-24T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-25T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-26T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-27T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-28T00:00:00.000' AS DateTime), N'15 210')

INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-29T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-30T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-08-31T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-01T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-02T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-03T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-04T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-05T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-06T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-07T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-08T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-09T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-10T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-11T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-12T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-13T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-14T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-15T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-16T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-17T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-18T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-19T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-20T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-21T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-22T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-23T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-24T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-25T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-26T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-27T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-28T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-29T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-09-30T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-01T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-02T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-03T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-04T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-05T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-06T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-07T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-08T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-09T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-10T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-11T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-12T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-13T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-14T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-15T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-16T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-17T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-18T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-19T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-20T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-21T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-22T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-23T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-24T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-25T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-26T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-27T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-28T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-29T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-30T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-10-31T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-01T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-02T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-03T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-04T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-05T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-06T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-07T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-08T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-09T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-10T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-11T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-12T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-13T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-14T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-15T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-16T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-17T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-18T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-19T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-20T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-21T00:00:00.000' AS DateTime), N'15 300')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-22T00:00:00.000' AS DateTime), N'15 330')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-23T00:00:00.000' AS DateTime), N'15 200')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-24T00:00:00.000' AS DateTime), N'15 210')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-25T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-26T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-27T00:00:00.000' AS DateTime), N'15 270')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-28T00:00:00.000' AS DateTime), N'15 410')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-29T00:00:00.000' AS DateTime), N'15 150')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-11-30T00:00:00.000' AS DateTime), N'15 420')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-01T00:00:00.000' AS DateTime), N'15 450')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-02T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-03T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-04T00:00:00.000' AS DateTime), N'15 400')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-05T00:00:00.000' AS DateTime), N'15 360')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-06T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-07T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-08T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-09T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-10T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-11T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-12T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-13T00:00:00.000' AS DateTime), N'14 920')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-14T00:00:00.000' AS DateTime), N'14 990')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-15T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-16T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-17T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-18T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-19T00:00:00.000' AS DateTime), N'14 770')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-20T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-21T00:00:00.000' AS DateTime), N'14 910')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-22T00:00:00.000' AS DateTime), N'14 860')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-23T00:00:00.000' AS DateTime), N'15 090')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-24T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-25T00:00:00.000' AS DateTime), N'15 080')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-26T00:00:00.000' AS DateTime), N'14 960')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-27T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-28T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-29T00:00:00.000' AS DateTime), N'14 890')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-30T00:00:00.000' AS DateTime), N'14 750')
INSERT Rates (RateDate, Total) VALUES (CAST(N'2014-12-31T00:00:00.000' AS DateTime), N'14 920')

INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Grocery Shopping', 100000, 250000, N'Br', 3, N'Week', N'Purse')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Clothes Shopping', 500000, 4000000, N'Br', 1, N'Month', N'Purse')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Transport', 100000, 200000, N'Br', 2, N'Month', N'Purse')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Rest', 200000, 500000, N'Br', 1, N'Week', N'Purse')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Study', 4000000, 6000000, N'Br', 3, N'Year', N'Purse')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'House Rent', 300, 350, N'Usd', 1, N'Monh', N'Safe')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Utilities', 5000000, 1500000, N'Br', 1, N'Month', N'Card')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Internet', 150000, 350000, N'Br', 1, N'Month', N'Card')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Phone', 1000000, 150000, N'Br', 1, N'Month', N'Card')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Phone, Internet', 50000, 70000, N'Br', 1, N'Month', N'Card')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Exp', N'Utilities, Phone', 10000, 15000, N'Br', 1, N'Month', N'Card')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Inc', N'Parents', 600, 1000, N'Usd', 6, N'Year', N'Safe')
INSERT UserTransactions ([Type], [Operation Name], [AmountMin], [AmountMax], [Currency], [Rate], [Period], [Account]) VALUES (N'Inc', N'Salary', 2000000, 4000000, N'Br', 2, N'Month', N'Card')
GO

CREATE PROCEDURE InsertInfoFromExcelTable
AS
INSERT INTO TypesOfMoney
(Name)
VALUES('Br')

INSERT INTO TypesOfMoney
(Name)
VALUES('Usd')

INSERT INTO Accounts
(Total, CreateDate, TypeOfMoneyId)
VALUES(0, CAST(N'2010-01-01T00:00:00.000' AS DateTime), (SELECT Id FROM TypesOfMoney WHERE Name = 'Br'))

INSERT INTO Accounts
(Total, CreateDate, TypeOfMoneyId)
VALUES(0, CAST(N'2010-01-01T00:00:00.000' AS DateTime), (SELECT Id FROM TypesOfMoney WHERE Name = 'Usd'))

INSERT INTO Denominations
(DifferenceToOne, TypeId)
SELECT 10000, TP.Id FROM TypesOfMoney TP WHERE TP.Name = 'Br'

INSERT INTO CrossCourses
(FirstTypeId, SecondTypeId, [Difference], CourseDate)
SELECT FT.Id, ST.Id, CAST(REPLACE(R.Total, ' ', '') AS float)/10000, R.RateDate
FROM Rates R
,(SELECT Id FROM TypesOfMoney WHERE Name = 'Usd') FT
,(SELECT Id FROM TypesOfMoney WHERE Name = 'Br') ST

INSERT INTO CrossCourses
(FirstTypeId, SecondTypeId, [Difference], CourseDate)
SELECT FT.Id, ST.Id, 10000/CAST(REPLACE(R.Total, ' ', '') AS float), R.RateDate
FROM Rates R
,(SELECT Id FROM TypesOfMoney WHERE Name = 'Br') FT
,(SELECT Id FROM TypesOfMoney WHERE Name = 'Usd') ST


INSERT INTO CategoriesOfTransaction
(Name)
SELECT UT.[Type]
FROM UserTransactions UT
GROUP BY UT.[Type]

--INSERT INTO TypesOfTransaction
--(Name, CategoryId)
--SELECT UT.[Operation Name], CT.Id
--FROM UserTransactions UT
--INNER JOIN CategoriesOfTransaction CT ON CT.Name = UT.[Type]

INSERT INTO TypesOfTransaction
(Name, CategoryId)
SELECT UTRes.Name, UTRes.CategoryId
FROM 
(
SELECT UT.[Operation Name] as Name
, C.Id as CategoryId
FROM UserTransactions UT
INNER JOIN CategoriesOfTransaction C ON C.Name = UT.[Type]
WHERE CHARINDEX(',', UT.[Operation Name]) = 0
UNION
SELECT LTRIM(RTRIM(res.Data)) as Name
, CT.Id as CategoryId
 FROM UserTransactions UsT
CROSS APPLY dbo.Split(UsT.[Operation Name], ',') res
INNER JOIN CategoriesOfTransaction CT ON CT.Name = UsT.[Type]
WHERE CHARINDEX(',', UsT.[Operation Name]) > 0 
) UTRes

INSERT INTO TypesOfTransaction
(Name, CategoryId)
SELECT 'Transfer From'
, CT.Id
FROM CategoriesOfTransaction CT
WHERE CT.Name = 'Exp'

INSERT INTO TypesOfTransaction
(Name, CategoryId)
SELECT 'Transfer To'
, CT.Id
FROM CategoriesOfTransaction CT
WHERE CT.Name = 'Inc'

INSERT INTO TypesOfTransaction
(Name, CategoryId)
SELECT 'Borrow from friend'
, CT.Id
FROM CategoriesOfTransaction CT
WHERE CT.Name = 'Inc'
GO

CREATE FUNCTION checkTypeName(@typeName varchar(4000))
RETURNS int
BEGIN 
	DECLARE @result int
	IF CHARINDEX(',', @typeName) > 0
		SET @result = (SELECT COUNT(*) FROM dbo.Split(@typeName, ','))
	ELSE
		SET @result = 1

	RETURN @result
	
END
GO
CREATE PROCEDURE GenerateTransactions(@period varchar(100))
AS
DECLARE @TempDate datetime
DECLARE @EndDate datetime
DECLARE @countDays int
DECLARE @partAmount int

IF @period = 'Week'
BEGIN
	SET @countDays = 7
END

IF @period = 'Month' OR @period = 'Monh'
BEGIN
	SET @countDays = 30
END

IF @period = 'Year'
BEGIN
	SET @countDays = 365
END

SET @EndDate = (SELECT TOP 1 RateDate FROM Rates ORDER BY RateDate DESC)
SET @TempDate = CAST(N'2010-01-01T00:00:00.000' AS DateTime)

WHILE @TempDate < @EndDate
BEGIN
	
	INSERT INTO Transactions
	(Amount, Note, CreateDate, TypeId, AccountId)
	SELECT CAST(dbo.generateRandomNumber(CAST(UT.AmountMin AS int), CAST(UT.AmountMax AS int)) AS money) AS Amount
	, UT.Account AS Note
	, DATEADD(DAY, Random.number, @TempDate) AS CreateDate
	, TT.Id AS TypeId
	, A.Id AS AccountId
	FROM UserTransactions UT
	CROSS APPLY dbo.generateTableRandomNumbers(UT.Rate, 0, @countDays) Random
	INNER JOIN TypesOfTransaction TT ON TT.Name = UT.[Operation Name] OR (CHARINDEX(',', UT.[Operation Name]) > 0 AND CHARINDEX(TT.Name, UT.[Operation Name]) > 0)
	INNER JOIN TypesOfMoney TM ON TM.Name = UT.Currency
	INNER JOIN Accounts A ON A.TypeOfMoneyId = TM.Id
	WHERE UT.Period = @period AND CHARINDEX(',', UT.[Operation Name]) = 0
	UNION
	SELECT CAST(dbo.generateRandomNumber(CAST(UT.AmountMin AS int), CAST(UT.AmountMax AS int)) AS money) AS Amount
	, UT.Account AS Note
	, DATEADD(DAY, Random.number, @TempDate) AS CreateDate
	, TT.Id AS TypeId
	, A.Id AS AccountId
	FROM 
		(SELECT LTRIM(RTRIM(res.Data)) as [Operation Name]
		, UsT.Rate AS Rate
		, UsT.Currency AS Currency
		, UsT.Period AS Period
		, UsT.Account AS Account
		, CAST(UsT.AmountMin / dbo.checkTypeName(UsT.[Operation Name]) AS float) AS AmountMin
		, CAST(UsT.AmountMax / dbo.checkTypeName(UsT.[Operation Name]) AS float) AS AmountMax
		FROM UserTransactions UsT
		CROSS APPLY dbo.Split(UsT.[Operation Name], ',') res
		WHERE CHARINDEX(',', UsT.[Operation Name]) > 0 
		) UT
	CROSS APPLY dbo.generateTableRandomNumbers(UT.Rate, 0, @countDays) Random
	INNER JOIN TypesOfTransaction TT ON TT.Name = UT.[Operation Name]
	INNER JOIN TypesOfMoney TM ON TM.Name = UT.Currency
	INNER JOIN Accounts A ON A.TypeOfMoneyId = TM.Id
	WHERE UT.Period = @period
	

	IF @period = 'Week'
	BEGIN
		SET @TempDate = DATEADD(WEEK, 1, @TempDate)
		IF (DATEADD(WEEK, 1, @TempDate) > @EndDate) AND (@TempDate < @EndDate)
		BEGIN
			SET @countDays = 5
		END
	END

	IF @period = 'Month' OR @period = 'Monh'
	BEGIN
		SET @TempDate = DATEADD(MONTH, 1, @TempDate)
	END

	IF @period = 'Year'
	BEGIN
		SET @TempDate = DATEADD(YEAR, 1, @TempDate)
	END
	
END
GO

CREATE PROCEDURE DenominationTransactions
AS
UPDATE Transactions
SET Amount = Amount/D.DifferenceToOne
FROM Accounts A
INNER JOIN TypesOfMoney TM ON TM.Id = A.TypeOfMoneyId
INNER JOIN Denominations D ON D.TypeId = TM.Id
WHERE AccountId = A.Id
GO

CREATE PROCEDURE ExecuteTransactions
AS
UPDATE Accounts
SET Total = TOTAL + T.Amount
FROM 
	(
		SELECT SUM(T.Amount) AS Amount
		, A.Id
		FROM Transactions T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Inc'
		INNER JOIN Accounts A ON A.Id = T.AccountId
		GROUP BY A.Id, CT.Name
	)T
INNER JOIN Accounts A ON A.Id = T.Id

UPDATE Accounts
SET Total = TOTAL - T.Amount
FROM 
	(
		SELECT SUM(T.Amount) AS Amount
		, A.Id
		FROM Transactions T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Exp'
		INNER JOIN Accounts A ON A.Id = T.AccountId
		GROUP BY A.Id, CT.Name
	)T
INNER JOIN Accounts A ON A.Id = T.Id
GO

CREATE PROCEDURE ExecuteTransfer(@AIdFrom int, @AIdTo int, @AmountFrom money, @AmountTo money,@Date datetime)
AS
INSERT INTO Transactions
(Amount, Note, CreateDate, TypeId, AccountId)
SELECT @AmountFrom AS Amount
, 'Transfer' AS Note
, @Date AS CreateDate
, TT.Id AS TypeId
, @AIdFrom AS AccountId
FROM TypesOfTransaction TT
WHERE TT.Name = 'Transfer From'

INSERT INTO Transactions
(Amount, Note, CreateDate, TypeId, AccountId)
SELECT @AmountTo AS Amount
, 'Transfer' AS Note
, @Date AS CreateDate
, TT.Id AS TypeId
, @AIdTo AS AccountId
FROM TypesOfTransaction TT
WHERE TT.Name = 'Transfer To'
GO

CREATE PROCEDURE ExecuteBorrowFromFriends(@amount money, @AId int, @Date datetime)
AS
CREATE TABLE #FriendList
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
)

INSERT INTO #FriendList
(Name)
VALUES('Alex')

INSERT INTO #FriendList
(Name)
VALUES('Max')

INSERT INTO #FriendList
(Name)
VALUES('Artem')

INSERT INTO #FriendList
(Name)
VALUES('Win in lotery')

INSERT INTO Transactions
(Amount, Note, CreateDate, TypeId, AccountId)
SELECT @amount AS Amount
, 'Borrow from ' + FL.Name AS Note
, @Date AS CreateDate
, TT.Id AS TypeId
, @AId AS AccountId
FROM TypesOfTransaction TT, #FriendList FL
WHERE TT.Name = 'Borrow from friend' AND FL.Id = dbo.generateRandomNumber(1, 4)

DROP TABLE #FriendList
GO

CREATE PROCEDURE CheckAndExecuteTransfers
AS
DECLARE @resultBr money
	DECLARE @resultUsd money
	DECLARE @incRes money
	DECLARE @excRes money
	DECLARE @counterDate datetime
	DECLARE @endDate datetime
	DECLARE @accountBr int
	DECLARE @accountUsd int
	DECLARE @courseConvert float
	DECLARE @convertFrom money
	DECLARE @convertTo money
	DECLARE @borrow money
	SET @accountBr = (SELECT A.Id FROM Accounts A
						INNER JOIN TypesOfMoney TP ON TP.Id = A.TypeOfMoneyId
						WHERE TP.Name = 'Br')
	SET @accountUsd = (SELECT A.Id FROM Accounts A
						INNER JOIN TypesOfMoney TP ON TP.Id = A.TypeOfMoneyId
						WHERE TP.Name = 'Usd')
	SET @counterDate = (SELECT TOP 1 CC.CourseDate FROM CrossCourses CC ORDER BY CC.CourseDate)
	SET @endDate = (SELECT TOP 1 CC.CourseDate FROM CrossCourses CC ORDER BY CC.CourseDate DESC)
	SET @resultBr = 0
	SET @resultUsd = 0
	SET @incRes = 0
	SET @excRes = 0
	WHILE @counterDate <= @endDate
	BEGIN
		SET @incRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountBr) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Inc'
		GROUP BY CT.Name)

		SET @excRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountBr) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Exp'
		GROUP BY CT.Name)

		IF @incRes IS NULL
		BEGIN
			SET @incRes = 0
		END
		
		IF @excRes IS NULL
		BEGIN
			SET @excRes = 0
		END

		SET @resultBr = @resultBr +  @incRes - @excRes

		SET @incRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountUsd) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Inc'
		GROUP BY CT.Name)

		SET @excRes = (SELECT SUM(T.Amount)
		FROM (SELECT* FROM Transactions WHERE CreateDate = @counterDate AND AccountId = @accountUsd) T
		INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
		INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId AND CT.Name = 'Exp'
		GROUP BY CT.Name)

		IF @incRes IS NULL
		BEGIN
			SET @incRes = 0
		END
		
		IF @excRes IS NULL
		BEGIN
			SET @excRes = 0
		END

		SET @resultUsd = @resultUsd +  @incRes - @excRes
		
		IF @resultBr < 0 AND @resultUsd > 0
		BEGIN
			SET @courseConvert = (SELECT CC.[Difference]
							FROM CrossCourses CC
							INNER JOIN TypesOfMoney TM ON TM.Id = CC.FirstTypeId 
							WHERE CC.CourseDate = @counterDate AND TM.Name = 'Usd')
			IF @resultUsd*@courseConvert > (-1)*@resultBr
				BEGIN 
					SET @convertFrom = CAST((-1)*@resultBr/@courseConvert AS money)
					SET @convertTo = CAST((-1)*@resultBr AS money)
				END
			ELSE 
				BEGIN 
					SET @convertFrom = @resultUsd
					SET @convertTo = CAST(@resultUsd*@courseConvert AS money)
				END
			
			SET @resultUsd = @resultUsd - @convertFrom
			SET @resultBr = @resultBr + @convertTo
			EXEC ExecuteTransfer @accountUsd, @accountBr, @convertFrom, @convertTo, @counterDate
		END
		ELSE IF @resultUsd < 0 AND @resultBr > 0
		BEGIN 
			SET @courseConvert = (SELECT CC.[Difference] 
								FROM CrossCourses CC
								INNER JOIN TypesOfMoney TM ON TM.Id = CC.FirstTypeId 
								WHERE CC.CourseDate = @counterDate AND TM.Name = 'Br')
			IF @resultBr*@courseConvert > (-1)*@resultUsd
				BEGIN 
					SET @convertFrom = CAST((-1)*@resultUsd/@courseConvert AS money)
					SET @convertTo = CAST((-1)*@resultUsd AS money)
				END
			ELSE 
				BEGIN 
					SET @convertFrom = @resultBr
					SET @convertTo = CAST(@resultBr*@courseConvert AS money)
				END
			
			SET @resultBr = @resultBr - @convertFrom
			SET @resultUsd = @resultUsd + @convertTo
			EXEC ExecuteTransfer @accountBr, @accountUsd, @convertFrom, @convertTo, @counterDate
		END

		IF @resultBr < 0
		BEGIN
			SET @borrow = CAST(@resultBr*(-1) as money)
			EXEC ExecuteBorrowFromFriends @borrow, @accountBr, @counterDate
			SET @resultBr = 0
		END

		IF @resultUsd < 0
		BEGIN
			SET @borrow = CAST(@resultUsd*(-1) as money)
			EXEC ExecuteBorrowFromFriends @borrow, @accountUsd, @counterDate
			SET @resultUsd = 0
		END
		SET @counterDate = DATEADD(day, 1, @counterDate)
		SET @incRes = 0
		SET @excRes = 0
	END
GO

Exec CreateTables
GO

Exec InsertExcelDataTables
GO

Exec InsertInfoFromExcelTable
GO

Exec GenerateTransactions 'Week'
GO

Exec GenerateTransactions 'Month'
GO

Exec GenerateTransactions 'Monh'
GO

Exec GenerateTransactions 'Year'
GO

Exec DenominationTransactions
GO

EXEC CheckAndExecuteTransfers
GO

Exec ExecuteTransactions
GO

--CREATE VIEW cashFlowView
--AS
--SELECT Res.[Date]
--, Res.AmountUsd AS UsdBalance
--, Res.AmountBr AS BlrBalance
--, CAST(Res.AmountBr*D.DifferenceToOne AS money) AS OldBlrBalance
--FROM dbo.getAmountAccounts() Res,
--(SELECT Id FROM TypesOfMoney WHERE Name = 'Br') TP
--INNER JOIN Denominations D ON D.TypeId = TP.Id
--GO

CREATE VIEW cashFlowView
AS
SELECT Result.[Date]
	, SUM(Result.UsdBalance) OVER (ORDER BY Result.[Date] rows unbounded preceding) as UsdBalance
	, SUM(Result.BlrBalance) OVER (ORDER BY Result.[Date] rows unbounded preceding) as BlrBalance
	, SUM(Result.OldBlrBalance) OVER (ORDER BY Result.[Date] rows unbounded preceding) as OldBlrBalance
FROM 
( 
	SELECT Res.[Date]
	, CAST(SUM(Res.UsdBalance) as money) as UsdBalance
	, CAST(SUM(Res.BlrBalance) as money) as BlrBalance
	, CAST(SUM(Res.OldBlrBalance) as money) as OldBlrBalance
	FROM
	(
		SELECT TD.[Date]
		, TD.UsdBalance
		, TD.BlrBalance
		, TD.BlrBalance*D.DifferenceToOne as OldBlrBalance
		FROM 
		(
			SELECT T.CreateDate as [Date]
			, CAST(CASE TM.Name  
						WHEN 'Usd' THEN SUM(T.Amount)
						ELSE 0
						END as money) as UsdBalance
			, CAST(CASE TM.Name  
						WHEN 'Br' THEN SUM(T.Amount)
						ELSE 0
						END as money) as BlrBalance
			, TM.Name
			FROM 
			(
				SELECT T.AccountId
				, T.CreateDate
				, CAST(CASE CT.Name
						WHEN 'Inc' THEN T.Amount
						ELSE T.Amount*(-1)
						END as money) as Amount
				FROM Transactions T
				INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
				INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId
			)T
			INNER JOIN Accounts A ON A.Id = T.AccountId
			INNER JOIN TypesOfMoney TM ON TM.Id = A.TypeOfMoneyId
			GROUP BY T.CreateDate, TM.Name
		)TD, Denominations D
		INNER JOIN TypesOfMoney TM ON TM.Id = D.TypeId
		UNION ALL
		SELECT DT.[Date]
		, 0 as UsdBalance
		, 0 as BlrBalance
		, 0 as OldBlrBalance
		FROM getDateTable() DT
	) Res
	GROUP BY Res.Date
) Result
GO

select*
from cashFlowView
-------------------------his transactions
IF OBJECT_ID('GetAccountTransactions') IS NOT NULL
DROP PROCEDURE GetAccountTransactions
GO
CREATE PROCEDURE GetAccountTransactions(@accountId int)
AS
IF @accountId > 0
BEGIN
	SELECT T.Id
	, T.CreateDate
	, T.Amount
	, TT.Id AS TypeID
	, TT.Name AS TypeName
	, CT.Id AS CategoryID
	, CT.Name AS CategoryName
	FROM Transactions T
	INNER JOIN Accounts A ON A.Id = T.AccountId
	INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
	INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId
	WHERE A.Id = @accountId
	ORDER BY T.CreateDate
END
ELSE
BEGIN
	SELECT T.Id
	, T.CreateDate
	, T.Amount
	, TT.Id AS TypeID
	, TT.Name AS TypeName
	, CT.Id AS CategoryID
	, CT.Name AS CategoryName
	FROM Transactions T
	INNER JOIN Accounts A ON A.Id = T.AccountId
	INNER JOIN TypesOfTransaction TT ON TT.Id = T.TypeId
	INNER JOIN CategoriesOfTransaction CT ON CT.Id = TT.CategoryId
	ORDER BY T.CreateDate
END
GO
-------------------------his account's balance
IF OBJECT_ID('GetAccounts') IS NOT NULL
DROP PROCEDURE GetAccounts
GO

CREATE PROCEDURE GetAccounts
AS
SELECT A.Id, A.Total
, TM.Id as TypeOfMoneyId
, TM.Name as TypeOfMoneyName
FROM Accounts A
INNER JOIN TypesOfMoney TM ON TM.Id = A.TypeOfMoneyId
GO

IF OBJECT_ID('GetTypesOfMoney') IS NOT NULL
DROP PROCEDURE GetTypesOfMoney
GO

CREATE PROCEDURE GetTypesOfMoney
AS
SELECT TM.Id
, TM.Name
FROM TypesOfMoney TM
GO

--------------------------------CashFlowView
IF OBJECT_ID('ExecuteCashFlowView') IS NOT NULL
DROP PROCEDURE ExecuteCashFlowView
GO

CREATE PROCEDURE ExecuteCashFlowView
AS
SELECT* 
FROM cashFlowView 
ORDER BY [Date]
GO

---------------------------------------------------GetCrossCourses

IF OBJECT_ID('GetCrossCourses') IS NOT NULL
DROP PROCEDURE GetCrossCourses
GO

CREATE PROCEDURE GetCrossCourses(@typeId int)
AS
IF @typeId > 0
BEGIN
	SELECT CC.Id AS Id
	, CC.[Difference]
	, CC.CourseDate AS [Date]
	, TM2.Id AS SecondId
	, TM2.Name AS SecondName
	FROM CrossCourses CC
	INNER JOIN TypesOfMoney TM ON TM.Id = CC.FirstTypeId
	INNER JOIN TypesOfMoney TM2 ON TM2.Id = CC.SecondTypeId
	WHERE TM.Id = @typeId
END
ELSE
BEGIN
	SELECT CC.Id AS Id
	, CC.[Difference]
	, CC.CourseDate AS [Date]
	, TM2.Id AS SecondId
	, TM2.Name AS SecondName
	FROM CrossCourses CC
	INNER JOIN TypesOfMoney TM ON TM.Id = CC.FirstTypeId
	INNER JOIN TypesOfMoney TM2 ON TM2.Id = CC.SecondTypeId
END
GO
---------------------------------------------------GetDenomination
IF OBJECT_ID('GetDenomination') IS NOT NULL
DROP PROCEDURE GetDenomination
GO

CREATE PROCEDURE GetDenomination(@typeId int)
AS
IF @typeId > 0
BEGIN
	SELECT D.Id
	, D.DifferenceToOne
	FROM Denominations D
	INNER JOIN TypesOfMoney TM ON TM.Id = D.TypeId
	WHERE TM.Id = @typeId
END
ELSE
BEGIN
	SELECT D.Id
	, D.DifferenceToOne
	FROM Denominations D
	INNER JOIN TypesOfMoney TM ON TM.Id = D.TypeId
END
GO



IF OBJECT_ID ( 'dbo.CreateTablesForEFMigration', 'P' ) IS NOT NULL   
    DROP PROCEDURE dbo.CreateTablesForEFMigration;  
GO

CREATE PROCEDURE CreateTablesForEFMigration
AS
IF OBJECT_ID('TypesOfMoney') IS NULL
  CREATE TABLE TypesOfMoney
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL
)

IF OBJECT_ID('Denominations') IS NULL
  CREATE TABLE Denominations
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	DifferenceToOne money NOT NULL,
	TypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id)
)

IF OBJECT_ID('CrossCourses') IS NULL
  CREATE TABLE CrossCourses
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	FirstTypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	SecondTypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	[Difference] float NOT NULL,
	CourseDate datetime NOT NULL default(GETDATE())
)


IF OBJECT_ID('CategoriesOfTransaction') IS NULL
  CREATE TABLE CategoriesOfTransaction
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name varchar(250) NOT NULL
)

IF OBJECT_ID('TypesOfTransaction') IS NULL
  CREATE TABLE TypesOfTransaction
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name varchar(250) NOT NULL,
	CategoryId int NOT NULL FOREIGN KEY REFERENCES CategoriesOfTransaction(Id)
)


IF OBJECT_ID('Accounts') IS NULL
  CREATE TABLE Accounts
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Total money NOT NULL default(0),
	TypeOfMoneyId int NOT NULL FOREIGN KEY REFERENCES TypesOfMoney(Id),
	CreateDate datetime NOT NULL default(GETDATE()),
	DeleteDate datetime NULL
)

IF OBJECT_ID('Transactions') IS NULL
  CREATE TABLE Transactions
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Amount money NOT NULL default(0),
	TypeId int NOT NULL FOREIGN KEY REFERENCES TypesOfTransaction(Id),
	AccountId int NOT NULL FOREIGN KEY REFERENCES Accounts(Id),
	Note varchar(250) NOT NULL default(''),
	CreateDate datetime NOT NULL default(GETDATE()),
	DeleteDate datetime NULL
)
GO