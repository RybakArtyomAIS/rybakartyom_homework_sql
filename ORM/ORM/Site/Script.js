//var url = "http://localhost:63750/TypeOfTransaction/";
//var oDataUrl = "http://localhost:63750/TypeOfTransactionModels/Types"
var url = "http://www.orm.webapi.loc/TypeOfTransaction/";
var oDataUrl = "http://www.orm.webapi.loc/TypeOfTransactionModels/Types?$filter=Id eq 2"
// var url = "http://localhost:60660/TypeOfTransaction/";
var getAllUrl = url + "GetAll";
var getByIdUrl = url + "GetById/";
var createUrl = url + "Create";
var updateUrl = url + "Update";
var deleteUrl = url + "Delete/";
var loginUrl = url + "Login";
var allTypes;
var currentTypes;
var elementCount = 5;


ExecuteGet();
console.log( "Update types list." );

function ShowError(data){
    if ( data.responseCode ){
        console.log( data.responseCode );
        alert('Error:' + data.statusText);
    }
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function ExecuteGet(){
    var data = { Login: "TestUser", Password: "12345" };
    $.post(loginUrl, data)
        .done(function(token){ 
            setCookie("api_key", token);
            alert("Authorize success!"); 
        })
        .fail(ShowError);
    $.get(getAllUrl)
    .done(SetTypeOfTransaction)
    .fail(ShowError);
}

function SetTypeOfTransaction(data){
    var i = 0;
    allTypes = data;
    while(i < data.length){
        AddHtmlElementToListOfTypes(data[i]);
        i++;
    }
    AddPagination();
}

function AddHtmlElementToListOfTypes(model){
    var htmlTable = document.getElementById('ListOfTypesId');
    var htmlTr = document.createElement("tr");
    var htmlTh = document.createElement("th");
    var text = document.createTextNode(model.Id);
    htmlTh.appendChild(text);
    htmlTr.appendChild(htmlTh);

    htmlTh = document.createElement("th");
    text = document.createTextNode(model.Name);
    htmlTh.appendChild(text);
    htmlTr.appendChild(htmlTh);
    
    htmlTh = document.createElement("th");
    var cat = "Exp";
    if (model.CategoryOfTransactionId == 2){
        cat = "Inc"
    }
    text = document.createTextNode(cat);
    htmlTh.appendChild(text);
    htmlTr.appendChild(htmlTh);

    htmlTable.appendChild(htmlTr);
    for (var i = 0; i < document.forms.length; i++) {
        if (document.forms[i].selectType != null){
            var option = new Option(model.Name, model.Id, true, true);
            document.forms[i].selectType.appendChild(option);
        }
    }
}

function AddPagination(){
    var ulHtml = document.getElementById("page");
    var count = allTypes.length/elementCount;
    for(var i = 1; i <= count; i++){
        var liHtml = document.createElement("li");
        var text = document.createTextNode(i);
        liHtml.appendChild(text);
        ulHtml.appendChild(liHtml);
        liHtml.onclick = ExecutePagination(i);
    }
    if(count < i && count > i - 1){
        var liHtml = document.createElement("li");
        var text = document.createTextNode(i);
        liHtml.appendChild(text);
        ulHtml.appendChild(liHtml);
        liHtml.onclick = ExecutePagination(i);
    }

}

function ExecutePagination(n){
    
}

function ClearHtmlElements(){
    var myNode = document.getElementById("ListOfTypesId");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }

    for (var i = 0; i < document.forms.length; i++) {
        if (document.forms[i].selectType != null){
            var htmlSelect = document.forms[i].selectType;
            while (htmlSelect.firstChild) {
                htmlSelect.removeChild(htmlSelect.firstChild);
            }
        }
    }
}

function CheckContainsType(typeName, categoryId){
    for(var i=0; i<allTypes.length; i++){
        if(allTypes[i].Name == typeName && allTypes[i].CategoryOfTransactionId == categoryId){
            alert("Type with the same type's name and selected category has been created. Change type name or category.");
            return true;
        }
    }
    return false;
}

function GetSelectedValue(selectList){
    for(var i=0; i<selectList.options.length;i++){
        var option = selectList.options[i];
        if(option.selected){
            return option.value;
        }
    }
}

function CreateNewType(){
    var form = document.forms.addForm;
    var selectCategoryValue = GetSelectedValue(form.selectCategory);
    
    if (CheckContainsType(form.typeName.value, selectCategoryValue)){
        return;
    }
    $.post( createUrl, { Name: form.typeName.value, CategoryOfTransactionId: selectCategoryValue })
        .done(function( id ) {
            alert( "Type has added successfull. Id: " + id );
        })
        .fail(ShowError);
}

function UpdateType(){
    var form = document.forms.updateForm;
    var selectCategoryValue = GetSelectedValue(form.selectCategory);
    var selectTypeValue  = GetSelectedValue(form.selectType);

    if (CheckContainsType(form.typeName.value, selectCategoryValue)){
        return;
    }
    var data = { Id: selectTypeValue, Name: form.typeName.value, CategoryOfTransactionId: selectCategoryValue };
    $.when($.post( updateUrl, data)
        .done(function(data) {
             alert( "Type has updated successfull." );
        })
        .fail(ShowError));
}

function DeleteType(){
    var form = document.forms.deleteForm;
    var selectTypeValue  = GetSelectedValue(form.selectType);
    $.when($.get(deleteUrl + selectTypeValue)
    .done(function(data){
        alert( "Type has deleted successfull." );
    })
    .fail(ShowError));
}