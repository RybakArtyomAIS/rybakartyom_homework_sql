﻿using ORM.BL.DTO;
using ORM.BL.DTO.Models;
using ORM.IoC;
using ORM.WebAPIConsole.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORM.WebAPIConsole.ApiControllers
{
    public class TypeOfTransactionController : ApiController
    {
        private readonly IAccountService _accountService;

        public TypeOfTransactionController()
        {
            IoContainer.SetEFConfig();
            _accountService = IoContainer.GetIAccountServiceInstance;
        }

        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            try
            {
                var typeOfTransactionsList = _accountService.GetTypesOfTransaction().Select(t => t.ToWebAPI()).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, typeOfTransactionsList);
            }
            catch (Exception exp)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exp);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            try
            {
                var model = _accountService.GetTypeOfTransactionById(id);
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            catch (Exception exp)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exp);
            }
        }

        [HttpPost]
        public HttpResponseMessage Create([FromBody]TypeOfTransactionWebAPIModel model)
        {
            try
            {
                var id = _accountService.CreateTypeOfTransaction(model.ToBL());
                return Request.CreateResponse(HttpStatusCode.OK, id);
            }
            catch (Exception exp)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exp);
            }
        }

        [HttpPost]
        public HttpResponseMessage Update(TypeOfTransactionWebAPIModel model)
        {
            try
            {
                _accountService.UpdateTypeOfTransaction(model.ToBL());
                return Request.CreateResponse(HttpStatusCode.OK, "");
            }
            catch (Exception exp)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exp);
            }
        }

        [HttpGet]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _accountService.DropTypeOfTransaction(id);
                return Request.CreateResponse(HttpStatusCode.OK, id);
            }
            catch (Exception exp)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exp);
            }
        }
    }
}
