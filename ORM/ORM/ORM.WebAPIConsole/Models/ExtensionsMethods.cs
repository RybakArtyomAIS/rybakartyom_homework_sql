﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM.BL.DTO.Models;

namespace ORM.WebAPIConsole.Models
{
    public static class ExtensionsMethods
    {
        public static TypeOfTransactionWebAPIModel ToWebAPI(this TypeOfTransactionModel model)
        {
            return new TypeOfTransactionWebAPIModel
            {
                Id = model.Id,
                Name = model.Name,
                CategoryOfTransactionId = model.CategoryOfTransactionId
            };
        }

        public static TypeOfTransactionModel ToBL(this TypeOfTransactionWebAPIModel model)
        {
            return new TypeOfTransactionModel
            {
                Id = model.Id,
                Name = model.Name,
                CategoryOfTransactionId = model.CategoryOfTransactionId
            };
        }
    }
}
