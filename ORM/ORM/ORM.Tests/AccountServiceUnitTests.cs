﻿using ORM.BL;
using ORM.BL.DTO;
using ORM.BL.DTO.Models;
using ORM.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ORM.Tests
{
    public class AccountServiceUnitTests
    {
        private IAccountService _accountService;

        private void InitializeAccountService()
        {
            IoContainer.SetEFConfig();
            _accountService = IoContainer.GetIAccountServiceInstance;
        }
        
        [Theory]
        [InlineData("TestCurrency")]
        public void CreateTypeOfMoneyTest(string name)
        {
            InitializeAccountService();
            var typeOfMoneyList = _accountService.GetTypesOfMoney();
            
            Assert.Empty(typeOfMoneyList.Where(t => t.Name == name));
            
            int id = _accountService.CreateTypeOfMoney(name);
            typeOfMoneyList = _accountService.GetTypesOfMoney();
            
            Assert.NotEmpty(typeOfMoneyList.Where(t => t.Name == name && t.Id == id));
            
            _accountService.DropTypeOfMoney(id);
            typeOfMoneyList = _accountService.GetTypesOfMoney();

            Assert.Empty(typeOfMoneyList.Where(t => t.Name == name && t.Id == id));
        }

        [Theory]
        [InlineData(1)]
        public void ShowAccountBalanceForBlrTest(int id)
        {
            InitializeAccountService();
            var accountModel = _accountService.ShowAccountBalance(id);

            Assert.True(accountModel.Id == id);
        }

        [Theory]
        [InlineData("Usd", "12/12/2012", 18000)]
        public void UpdateCrossCourseByDateTest(string typeOfMoneyName, DateTime date, double difference)
        {
            InitializeAccountService();
            var crossCourse = _accountService.GetCrossCourseByDate(date, typeOfMoneyName);

            var test = crossCourse.Date == date && crossCourse.SecondTypeOfMoneyName == typeOfMoneyName && crossCourse.Difference != difference;
            Assert.True(crossCourse.Date == date && crossCourse.SecondTypeOfMoneyName == typeOfMoneyName && crossCourse.Difference != difference);

            var oldDiff = crossCourse.Difference;
            _accountService.UpdateCrossCourse(crossCourse.Id, difference);
            crossCourse = _accountService.GetCrossCourseByDate(date, typeOfMoneyName);

            Assert.True(crossCourse.Difference == difference);

            _accountService.UpdateCrossCourse(crossCourse.Id, oldDiff);
            crossCourse = _accountService.GetCrossCourseByDate(date, typeOfMoneyName);
            
            Assert.True(crossCourse.Difference == oldDiff);
        }

        [Theory]
        [InlineData("Br", "11/11/2011")]
        public void ShowCrossCourseTest(string typeOfMoneyName, DateTime date)
        {
            InitializeAccountService();
            var crossCourse = _accountService.GetCrossCourseByDate(date, typeOfMoneyName);

            Assert.NotNull(crossCourse);
            Assert.True(crossCourse.Date == date && crossCourse.SecondTypeOfMoneyName == typeOfMoneyName);
        }

        [Theory]
        [InlineData("Br", 100, "Win in Lottery", "Inc")]
        public void CreateTransactionTest(string typeOfMoneyName, decimal amount, string type, string category)
        {
            InitializeAccountService();
            var date = new DateTime(2011, 11, 11);
            var transaction = _accountService.GetTransactions().FirstOrDefault(t => t.Date == date && t.Total == amount && t.TypeOfMoneyName == typeOfMoneyName);

            Assert.Null(transaction);

            int id = _accountService.CreateTransaction(typeOfMoneyName, date, amount, type, category);
            transaction = _accountService.GetTransactionById(id);

            Assert.NotNull(transaction);
            Assert.True(transaction.Date == date && transaction.Total == amount && transaction.TypeName == type && transaction.CategoryName == category);

            _accountService.DropTransaction(transaction.Id);
            transaction = _accountService.GetTransactionById(id);

            Assert.Null(transaction);
        }

        [Theory]
        [InlineData("Br", "11/11/2011", 100, "Win in Lottery", "Inc")]
        public void UpdateTransactionTest(string typeOfMoneyName, DateTime date, decimal amount, string type, string category)
        {
            InitializeAccountService();
            var transaction = _accountService.GetTransactions().FirstOrDefault(t => t.Date == date && t.Total == amount && t.TypeOfMoneyName == typeOfMoneyName);

            Assert.Null(transaction);

            int id = _accountService.CreateTransaction(typeOfMoneyName, date, amount, type, category);
            transaction = _accountService.GetTransactionById(id);

            Assert.NotNull(transaction);

            _accountService.UpdateTransaction(transaction.Id, new TransactionModel
            { 
                Date = transaction.Date, 
                Total = transaction.Total*10, 
                AccountId = transaction.AccountId, 
                TypeOfTransactionId = transaction.TypeOfTransactionId
            });
            var updateTransaction = _accountService.GetTransactionById(id);

            Assert.True(transaction.Date == updateTransaction.Date && transaction.Total*10 == updateTransaction.Total);

            _accountService.DropTransaction(id);
            transaction = _accountService.GetTransactionById(id);

            Assert.Null(transaction);
        }

        [Theory]
        [InlineData(256)]
        public void ShowTypeOfTransactionTest(int transactionId)
        {
            InitializeAccountService();
            var transaction = _accountService.GetTransactionById(transactionId);

            Assert.NotNull(transaction);

            var typeOfTransaction = _accountService.GetTypeOfTransactionById(transaction.TypeOfTransactionId);

            Assert.NotNull(typeOfTransaction);
            Assert.True(typeOfTransaction.Id == transaction.TypeOfTransactionId && typeOfTransaction.Name == transaction.TypeName);
            
        }

        [Theory]
        [InlineData(1, 100, "Win in Lottery", "Inc")]
        public void UpdateAccountBalance(int accountId, decimal amount, string type, string category)
        {
            InitializeAccountService();
            var account = _accountService.ShowAccountBalance(accountId);
            Assert.NotNull(account);
            var oldBalance = account.Amount;
            int transactionId = _accountService.CreateTransaction(account.TypeOfMoneyName, DateTime.Now.Date, amount, type, category);
            _accountService.UpdateAccountBalance(accountId);
            account = _accountService.ShowAccountBalance(accountId);

            Assert.Equal<decimal>(account.Amount, oldBalance + amount);

            _accountService.DropTransaction(transactionId);
            _accountService.UpdateAccountBalance(accountId);
            account = _accountService.ShowAccountBalance(accountId);

            Assert.Equal<decimal>(account.Amount, oldBalance);
        }
    }
}
