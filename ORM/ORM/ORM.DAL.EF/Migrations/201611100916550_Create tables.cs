namespace ORM.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Createtables : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Accounts",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Total = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            TypeOfMoneyId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.TypesOfMoney", t => t.TypeOfMoneyId, cascadeDelete: true)
            //    .Index(t => t.TypeOfMoneyId);
            
            //CreateTable(
            //    "dbo.Transactions",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            CreateDate = c.DateTime(nullable: false),
            //            Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            TypeId = c.Int(nullable: false),
            //            AccountId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: true)
            //    .ForeignKey("dbo.TypesOfTransaction", t => t.TypeId, cascadeDelete: true)
            //    .Index(t => t.TypeId)
            //    .Index(t => t.AccountId);
            
            //CreateTable(
            //    "dbo.TypesOfTransaction",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(),
            //            CategoryId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.CategoriesOfTransaction", t => t.CategoryId, cascadeDelete: true)
            //    .Index(t => t.CategoryId);
            
            //CreateTable(
            //    "dbo.CategoriesOfTransaction",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.TypesOfMoney",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.CrossCourses",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Difference = c.Double(nullable: false),
            //            CourseDate = c.DateTime(nullable: false),
            //            FirstTypeId = c.Int(nullable: false),
            //            SecondTypeId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.TypesOfMoney", t => t.FirstTypeId, cascadeDelete: true)
            //    .ForeignKey("dbo.TypesOfMoney", t => t.SecondTypeId, cascadeDelete: true)
            //    .Index(t => t.FirstTypeId)
            //    .Index(t => t.SecondTypeId);
            
            //CreateTable(
            //    "dbo.Denominations",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            DifferenceToOne = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            TypeId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.TypesOfMoney", t => t.TypeId, cascadeDelete: true)
            //    .Index(t => t.TypeId);

            var sql = "EXEC CreateTablesForEFMigration";
            Sql(sql);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "TypeOfMoneyId", "dbo.TypesOfMoney");
            DropForeignKey("dbo.Denominations", "TypeId", "dbo.TypesOfMoney");
            DropForeignKey("dbo.CrossCourses", "SecondTypeId", "dbo.TypesOfMoney");
            DropForeignKey("dbo.CrossCourses", "FirstTypeId", "dbo.TypesOfMoney");
            DropForeignKey("dbo.Transactions", "TypeId", "dbo.TypesOfTransaction");
            DropForeignKey("dbo.TypesOfTransaction", "CategoryId", "dbo.CategoriesOfTransaction");
            DropForeignKey("dbo.Transactions", "AccountId", "dbo.Accounts");
            DropIndex("dbo.Denominations", new[] { "TypeId" });
            DropIndex("dbo.CrossCourses", new[] { "SecondTypeId" });
            DropIndex("dbo.CrossCourses", new[] { "FirstTypeId" });
            DropIndex("dbo.TypesOfTransaction", new[] { "CategoryId" });
            DropIndex("dbo.Transactions", new[] { "AccountId" });
            DropIndex("dbo.Transactions", new[] { "TypeId" });
            DropIndex("dbo.Accounts", new[] { "TypeOfMoneyId" });
            DropTable("dbo.Denominations");
            DropTable("dbo.CrossCourses");
            DropTable("dbo.TypesOfMoney");
            DropTable("dbo.CategoriesOfTransaction");
            DropTable("dbo.TypesOfTransaction");
            DropTable("dbo.Transactions");
            DropTable("dbo.Accounts");
        }
    }
}
