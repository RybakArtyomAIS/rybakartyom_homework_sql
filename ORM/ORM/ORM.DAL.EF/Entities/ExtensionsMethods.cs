﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM.DAL.DTO.Models;

namespace ORM.DAL.EF.Entities
{
    public static class ExtensionsMethods
    {
        public static AccountDAL ToDAL(this AccountEF entity)
        {
            var result = new AccountDAL
            {
                Id = entity.Id,
                Amount = entity.Total,
                TypeOfMoneyId = entity.TypeOfMoneyId,
                TransactionsList = entity.TransactionsList.Select(ToDAL).ToList(), 
                TypeOfMoneyName = entity.TypeOfMoney != null ? entity.TypeOfMoney.Name : ""
            };
            return result;
        }

        public static AccountEF ToEntity(this AccountDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //var result = new AccountEF
            //{
            //    Id = dal.Id,
            //    Amount = dal.Amount,
            //    TypeOfMoney = dal.TypeOfMoney.ToEntity()
            //};
            //result.TransactionList = dal.TransactionsList.Select(item => new TransactionEF
            //{
            //    Id = item.Id,
            //    Date = item.Date,
            //    Total = item.Total,
            //    AccountEF = result,
            //    TypeOfTransactionEF = item.TypeOfTransaction.ToEntity()
            //}).ToList();
            //return result;
        }

        public static TransactionDAL ToDAL(this TransactionEF entity)
        {
            return new TransactionDAL
            {
                Id = entity.Id,
                Date = entity.CreateDate,
                Total = entity.Amount,
                AccountId = entity.AccountId,
                TypeOfTransactionId = entity.TypeId,
                CategoryName = entity.TypeOfTransaction != null && entity.TypeOfTransaction.Category != null ? entity.TypeOfTransaction.Category.Name : "",
                TypeName = entity.TypeOfTransaction != null ? entity.TypeOfTransaction.Name : "",
                TypeOfMoneyName = entity.Account != null && entity.Account.TypeOfMoney != null ? entity.Account.TypeOfMoney.Name : ""
            };
        }

        public static TransactionEF ToEntity(this TransactionDAL dal)
        {
            return new TransactionEF
            {
                Id = dal.Id,
                AccountId = dal.AccountId,
                Amount = dal.Total,
                CreateDate = dal.Date,
                TypeId = dal.TypeOfTransactionId
            };
        }


        public static TypeOfTransactionDAL ToDAL(this TypeOfTransactionEF entity)
        {
            return new TypeOfTransactionDAL
            {
                Id = entity.Id,
                Name = entity.Name,
                CategoryOfTransactionId = entity.CategoryId
            };
        }

        public static TypeOfTransactionEF ToEntity(this TypeOfTransactionDAL dal)
        {
            return new TypeOfTransactionEF
            {
                Name = dal.Name,
                CategoryId = dal.CategoryOfTransactionId,
                Id = dal.Id
            };
        }


        public static CategoryOfTransactionDAL ToDAL(this CategoryOfTransactionEF entity)
        {
            return new CategoryOfTransactionDAL
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public static CategoryOfTransactionEF ToEntity(this CategoryOfTransactionDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new CategoryOfTransactionEF
            //{
            //    Id = dal.Id,
            //    Name = dal.Name
            //};
        }


        public static TypeOfMoneyDAL ToDAL(this TypeOfMoneyEF entity)
        {
            var result = new TypeOfMoneyDAL
            {
                Id = entity.Id,
                Name = entity.Name,
                CrossCourseList = entity.CrossCourses.Select(ToDAL).ToList(),
                DenominationList = entity.Denominations.Select(ToDAL).ToList()
            };
            return result;
        }

        public static TypeOfMoneyEF ToEntity(this TypeOfMoneyDAL dal)
        {
            return new TypeOfMoneyEF
            {
                Name = dal.Name
            };
        }


        public static CrossCourseDAL ToDAL(this CrossCourseEF entity)
        {
            return new CrossCourseDAL
            {
                Id = entity.Id,
                Difference = entity.Difference,
                Date = entity.CourseDate,
                FirstTypeOfMoneyId = entity.FirstTypeId,
                FirstTypeOfMoneyName = entity.FirstTypeOfMoney.Name ?? "",
                SecondTypeOfMoneyId = entity.SecondTypeId, 
                SecondTypeOfMoneyName = entity.SecondTypeOfMoney.Name ?? ""
            };
        }

        public static CrossCourseEF ToEntity(this CrossCourseDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new CrossCourseEF
            //{
            //    Id = dal.Id,
            //    Difference = dal.Difference,
            //    Date = dal.Date,
            //    FirstTypeOfMoneyEF = dal.FirstTypeOfMoney.ToEntity(), 
            //    SecondTypeOfMoneyId = dal.SecondTypeOfMoneyId, 
            //    SecondTypeOfMoneyName = dal.SecondTypeOfMoneyName
            //};
        }


        public static DenominationDAL ToDAL(this DenominationEF entity)
        {
            return new DenominationDAL
            {
                Id = entity.Id,
                DifferenceToOne = entity.DifferenceToOne,
                TypeOfMoneyId = entity.TypeId
            };
        }

        public static DenominationEF ToEntity(this DenominationDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
            //return new DenominationEF
            //{
            //    Id = dal.Id,
            //    DifferenceToOne = dal.DifferenceToOne,
            //    TypeOfMoneyEF = dal.TypeOfMoney.ToEntity()
            //};
        }

        public static CashFlowItemDAL ToDAL(this CashFlowItemEF entity)
        {
            return new CashFlowItemDAL
            {
                Date = entity.Date,
                UsdBalance = entity.UsdBalance,
                BlrBalance = entity.BlrBalance,
                OldBlrBalance = entity.OldBlrBalance
            };
        }

        public static CashFlowItemEF ToEntity(this CashFlowItemDAL dal)
        {
            throw new Exception("Extension method DAL to EF doesn't make");
        //    return new CashFlowItemEF
        //    {
        //        Date = dal.Date,
        //        UsdBalance = dal.UsdBalance,
        //        BlrBalance = dal.BlrBalance,
        //        OldBlrBalance = dal.OldBlrBalance
        //    };
        }

    }
}
