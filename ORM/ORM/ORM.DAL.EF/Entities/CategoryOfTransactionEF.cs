﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.EF.Entities
{
    public class CategoryOfTransactionEF
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<TypeOfTransactionEF> Types { get; set; }
    }
}
