﻿using System;
namespace ORM.DAL.EF.Entities
{

    public class CrossCourseEF
    {
        public int Id { get; set; }

        public double Difference { get; set; }

        public DateTime CourseDate { get; set; }

        public virtual TypeOfMoneyEF SecondTypeOfMoney { get; set; }

        public virtual TypeOfMoneyEF FirstTypeOfMoney { get; set; }

        public int FirstTypeId { get; set; }

        public int SecondTypeId { get; set; }
    }
}