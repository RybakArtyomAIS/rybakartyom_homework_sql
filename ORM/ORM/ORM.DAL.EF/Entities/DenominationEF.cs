﻿namespace ORM.DAL.EF.Entities
{
    public class DenominationEF
    {
        public int Id { get; set; }

        public decimal DifferenceToOne { get; set; }

        public virtual TypeOfMoneyEF TypeOfMoney { get; set; }

        public int TypeId { get; set; }
    }
}