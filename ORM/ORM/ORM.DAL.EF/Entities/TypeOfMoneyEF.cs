﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.EF.Entities
{
    public class TypeOfMoneyEF
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<DenominationEF> Denominations { get; set; }

        public virtual ICollection<CrossCourseEF> CrossCourses { get; set; }

        public virtual ICollection<CrossCourseEF> CrossCoursesSecond { get; set; }

        public virtual ICollection<AccountEF> Accounts { get; set; }
    }
}
