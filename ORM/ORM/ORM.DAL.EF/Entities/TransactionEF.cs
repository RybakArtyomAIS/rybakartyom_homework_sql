﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.EF.Entities
{

    public class TransactionEF
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public decimal Amount { get; set; }

        public virtual TypeOfTransactionEF TypeOfTransaction { get; set; }
        
        public int TypeId { get; set; }

        public int AccountId { get; set; }

        public virtual AccountEF Account { get; set; }
    }
}
