﻿using ORM.DAL.EF.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL.EF.FluentAPIConfigs
{
    public class AccountConfig: EntityTypeConfiguration<AccountEF>
    {
        public AccountConfig()
        {
            ToTable("Accounts").HasKey(a => a.Id);
            HasRequired<TypeOfMoneyEF>(a => a.TypeOfMoney).WithMany(t => t.Accounts).HasForeignKey(a => a.TypeOfMoneyId);
        }
    }

    public class TransactionConfig: EntityTypeConfiguration<TransactionEF>
    {
        public TransactionConfig()
        {
            ToTable("Transactions").HasKey(t => t.Id);
            HasRequired<AccountEF>(t => t.Account).WithMany(a => a.TransactionsList).HasForeignKey(t => t.AccountId);
            HasRequired<TypeOfTransactionEF>(t => t.TypeOfTransaction).WithMany(t => t.Transactions).HasForeignKey(t => t.TypeId);
        }
    }

    public class CategoryConfig : EntityTypeConfiguration<CategoryOfTransactionEF>
    {
        public CategoryConfig()
        {
            ToTable("CategoriesOfTransaction").HasKey(c => c.Id);
        }
    }

    public class TypeOfTransactionConfig : EntityTypeConfiguration<TypeOfTransactionEF>
    {
        public TypeOfTransactionConfig()
        {
            ToTable("TypesOfTransaction").HasKey(t => t.Id);
            HasRequired<CategoryOfTransactionEF>(t => t.Category).WithMany(c => c.Types).HasForeignKey(t => t.CategoryId);
        }
    }

    public class TypeOfMoneyConfig : EntityTypeConfiguration<TypeOfMoneyEF>
    {
        public TypeOfMoneyConfig()
        {
            ToTable("TypesOfMoney").HasKey(t => t.Id);
        }
    }

    public class DenominationConfig : EntityTypeConfiguration<DenominationEF>
    {
        public DenominationConfig()
        {
            ToTable("Denominations").HasKey(t => t.Id);
            HasRequired<TypeOfMoneyEF>(d => d.TypeOfMoney).WithMany(t => t.Denominations).HasForeignKey(d => d.TypeId);
        }
    }

    public class CrossCourseConfig : EntityTypeConfiguration<CrossCourseEF>
    {
        public CrossCourseConfig()
        {
            ToTable("CrossCourses").HasKey(c => c.Id);
            HasRequired<TypeOfMoneyEF>(c => c.FirstTypeOfMoney).WithMany(t => t.CrossCourses).HasForeignKey(c => c.FirstTypeId);
            HasRequired<TypeOfMoneyEF>(c => c.SecondTypeOfMoney).WithMany(t => t.CrossCoursesSecond).HasForeignKey(c => c.SecondTypeId);
        }
    }
}
