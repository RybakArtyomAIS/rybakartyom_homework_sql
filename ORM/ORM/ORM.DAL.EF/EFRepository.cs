﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using ORM.DAL.DTO;
using ORM.DAL.DTO.Models;
using ORM.DAL.EF.Entities;

namespace ORM.DAL.EF
{
    public class EFRepository : IRepository
    {
        private readonly AccountContext _accountContext;
        public EFRepository()
        {
            _accountContext = new AccountContext();
        }

        public List<TypeOfMoneyDAL> GetTypesOfMoney()
        {
            //var test = _accountContext.TypesOfMoney.Include(t => t.Denominations).Include(t => t.CrossCourses).Include(t => t.Accounts).ToList();
            //return test.Select(t => t.ToDAL()).ToList();
            var typesOfMoney = _accountContext.TypesOfMoney.Include(t => t.Denominations).Include(t => t.CrossCourses).ToList();
            return typesOfMoney.Select(t => t.ToDAL()).ToList();
        }


        public List<AccountDAL> GetAccounts()
        {
            var accounts = _accountContext.Accounts.Include(a => a.TransactionsList.Select(t => t.TypeOfTransaction.Category)).Include(a => a.TypeOfMoney).ToList();
            return accounts.Select(a => a.ToDAL()).ToList();
            //var test = _accountContext.Accounts.Include(a => a.TypeOfMoney).Include(a => a.TransactionsList).ToList();
            //return _accountContext.Accounts.ToList().Select(item => item.ToDAL()).ToList();
        }

        public List<CashFlowItemDAL> GetCashFlowView()
        {
            var result = new List<CashFlowItemDAL>();
            var timer = new Stopwatch();
            timer.Start();
            var currentDate = _accountContext.CrossCourses.OrderBy(c => c.CourseDate).First().CourseDate;
            var endDate = _accountContext.CrossCourses.OrderByDescending(c => c.CourseDate).First().CourseDate;
            decimal usdBalance = 0;
            decimal blrBalance = 0;
            var denomination = _accountContext.Denominations.Include(d => d.TypeOfMoney).First(d => d.TypeOfMoney.Name == "Br").DifferenceToOne;
            var dateList = new List<DateTime>();
            while (currentDate <= endDate)
            {
                dateList.Add(currentDate);
                currentDate = currentDate.AddDays(1);
            }
            var res = _accountContext.Transactions.Include(t => t.Account.TypeOfMoney).Include(t => t.TypeOfTransaction.Category)
                .GroupBy(t => t.CreateDate)
                .Select(g => new CashFlowItemDAL
                {
                    Date = g.Key,
                    UsdBalance = g.Count(t => t.Account.TypeOfMoney.Name == "Usd") > 0 ? g.Where(t => t.Account.TypeOfMoney.Name == "Usd").Sum(t => t.TypeOfTransaction.Category.Name == "Inc" ? t.Amount : t.Amount * (-1)) : 0,
                    BlrBalance = g.Count(t => t.Account.TypeOfMoney.Name == "Br") > 0 ? g.Where(t => t.Account.TypeOfMoney.Name == "Br").Sum(t => t.TypeOfTransaction.Category.Name == "Inc" ? t.Amount : t.Amount * (-1)) : 0
                }).ToList()
                .Union(dateList.Select(d => new CashFlowItemDAL { Date = d, UsdBalance = 0, BlrBalance = 0 }))
                .GroupBy(c => c.Date)
                .OrderBy(g => g.Key)
                .Select(g =>
                {
                    usdBalance += g.Sum(c => c.UsdBalance);
                    blrBalance += g.Sum(c => c.BlrBalance);
                    return new CashFlowItemDAL
                    {
                        Date = g.Key.Date,
                        UsdBalance = usdBalance,
                        BlrBalance = blrBalance,
                        OldBlrBalance = blrBalance * denomination
                    };
                }).ToList();
            timer.Stop();
            res.OrderByDescending(item => item.Date).First().Time = timer.Elapsed;
            return res;
        }



        public List<TransactionDAL> GetTransactions(int? accountId)
        {
            //throw new NotImplementedException();
            return _accountContext.Transactions.Include(t => t.Account.TypeOfMoney).Include(t => t.TypeOfTransaction.Category).Where(t => accountId != null ? t.AccountId == accountId : true).ToList().Select(t => t.ToDAL()).ToList();
        }

        public List<CrossCourseDAL> GetCrossCourses()
        {
            return _accountContext.CrossCourses
                .Include(c => c.FirstTypeOfMoney)
                .Include(c => c.SecondTypeOfMoney)
                .ToList()
                .Select(c => c.ToDAL())
                .ToList();
        }

        public void SetTypeOfMoney(TypeOfMoneyDAL typeOfMoney)
        {
            _accountContext.TypesOfMoney.Add(typeOfMoney.ToEntity());
            _accountContext.SaveChanges();
        }

        public List<TypeOfTransactionDAL> GetTypesTransaction()
        {
            return _accountContext.TypesOfTransaction.ToList().Select(t => t.ToDAL()).ToList();
        }

        public void SetAccount(AccountDAL account)
        {
            _accountContext.Accounts.Add(account.ToEntity());
            _accountContext.SaveChanges();
        }

        public int CreateTypeOfMoney(TypeOfMoneyDAL typeOfMoneyDAL)
        {
            var typeOfMoney = typeOfMoneyDAL.ToEntity();
            _accountContext.TypesOfMoney.Add(typeOfMoney);
            _accountContext.SaveChanges();
            return typeOfMoney.Id;
        }

        public void DropTypeOfMoney(int id)
        {
            var account = _accountContext.TypesOfMoney.FirstOrDefault(t => t.Id == id);
            if (account != null)
            {
                _accountContext.TypesOfMoney.Remove(account);
                _accountContext.SaveChanges();
            }
        }

        public void UpdateCrossCourse(int id, CrossCourseDAL crossCourse)
        {
            var crossCourseEf = _accountContext.CrossCourses.FirstOrDefault(c => c.Id == id);
            if (crossCourse != null)
            {
                crossCourseEf.CourseDate = crossCourse.Date;
                crossCourseEf.Difference = crossCourse.Difference;
                crossCourseEf.FirstTypeId = crossCourse.FirstTypeOfMoneyId;
                crossCourseEf.SecondTypeId = crossCourse.SecondTypeOfMoneyId;
                _accountContext.SaveChanges();
            }
        }

        public int CreateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            var typeOfTransaction = typeOfTransactionDAL.ToEntity();
            _accountContext.TypesOfTransaction.Add(typeOfTransaction);
            _accountContext.SaveChanges();
            return typeOfTransaction.Id;
        }

        public void DropTransaction(int id)
        {
            var transaction = _accountContext.Transactions.FirstOrDefault(t => t.Id == id);
            if (transaction != null)
            {
                _accountContext.Transactions.Remove(transaction);
                _accountContext.SaveChanges();
            }
        }

        public void UpdateTransaction(int id, TransactionDAL transactionDAL)
        {
            var transaction = _accountContext.Transactions.FirstOrDefault(t => t.Id == id);
            if (transaction != null)
            {
                transaction.CreateDate = transactionDAL.Date;
                transaction.Amount = transactionDAL.Total;
                transaction.TypeId = transactionDAL.TypeOfTransactionId;
                transaction.AccountId = transactionDAL.AccountId;
                //transaction.TypeOfTransaction = _accountContext.TypesOfTransaction.FirstOrDefault(t => t.Id == transactionDAL.Id) ?? null;
                //transaction.Account = _accountContext.Accounts.FirstOrDefault(a => a.Id == transactionDAL.Id) ?? null;
                _accountContext.SaveChanges();
            }
        }

        public int CreateTransactions(TransactionDAL transactionDAL)
        {
            var transaction = transactionDAL.ToEntity();
            _accountContext.Transactions.Add(transaction);
            _accountContext.SaveChanges();
            return transaction.Id;
        }


        public List<CategoryOfTransactionDAL> GetCategoriesOfTransaction()
        {
            return _accountContext.CategoriesOfTransaction.ToList().Select(c => c.ToDAL()).ToList();
        }


        public void UpdateAccountBalance(int accountId)
        {
            var account = _accountContext.Accounts.FirstOrDefault(a => a.Id == accountId);
            if (account != null)
            {
                account.Total = Math.Round(_accountContext.Transactions
                    .Include(t => t.TypeOfTransaction.Category)
                    .Where(t => t.AccountId == account.Id)
                    .Select(t => t.TypeOfTransaction.Category.Name == "Inc" ? t.Amount : -1 * t.Amount)
                    .Sum(), 2);
                _accountContext.SaveChanges();
            }
        }

        public void UpdateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            var typeOfTransaction = _accountContext.TypesOfTransaction.FirstOrDefault(t => t.Id == typeOfTransactionDAL.Id);
            if (typeOfTransaction != null)
            {
                typeOfTransaction.Name = typeOfTransactionDAL.Name;
                if (typeOfTransaction.CategoryId != typeOfTransactionDAL.CategoryOfTransactionId)
                {
                    var category = _accountContext.CategoriesOfTransaction.First(c => c.Id == typeOfTransactionDAL.CategoryOfTransactionId);
                    typeOfTransaction.Category = category;
                    typeOfTransaction.CategoryId = typeOfTransactionDAL.CategoryOfTransactionId;
                }
                _accountContext.SaveChanges();
            }
        }

        public void DropTypeOfTransaction(int id)
        {
            var typeOfTransaction = _accountContext.TypesOfTransaction.Include(t => t.Transactions).FirstOrDefault(t => t.Id == id);
            if (typeOfTransaction != null)
            {
                if (typeOfTransaction.Transactions != null && typeOfTransaction.Transactions.Any())
                {
                    _accountContext.Transactions.RemoveRange(typeOfTransaction.Transactions);
                    _accountContext.SaveChanges();
                }
                _accountContext.TypesOfTransaction.Remove(typeOfTransaction);
                _accountContext.SaveChanges();
            }
        }



        public IQueryable<TypeOfTransactionDAL> TypesOfTransaction
        {
            get
            {
                return _accountContext.TypesOfTransaction.Select(t => new TypeOfTransactionDAL
            {
                Id = t.Id,
                Name = t.Name,
                CategoryOfTransactionId = t.CategoryId
            });
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
