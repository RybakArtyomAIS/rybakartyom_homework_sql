﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORM.WebAPI.Models
{
    public class LoginWebAPIModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}