﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORM.WebAPI.Models
{
    public class TypeOfTransactionWebAPIModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryOfTransactionId { get; set; }
    }
}