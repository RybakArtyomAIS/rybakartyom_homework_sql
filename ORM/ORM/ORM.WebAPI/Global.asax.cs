﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Routing;
using ORM.WebAPI.Infrastructure;
using ORM.IoC;
using ORM.WebAPI.App_Start;
namespace ORM.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            IoContainer.SetEFConfig();
            GlobalConfiguration.Configuration.Filters.Add(new JwtCustomAutorizeAttribute());
            GlobalConfiguration.Configuration.Filters.Add(new CustomExceptionFilter());
            GlobalConfiguration.Configuration.Filters.Add(new LoggerActionFilter());
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
