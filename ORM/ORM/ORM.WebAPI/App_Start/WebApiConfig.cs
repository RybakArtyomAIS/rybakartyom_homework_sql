﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using ORM.WebAPI.Models;
using ORM.BL.DTO.Models;
using System.Web.Http.OData.Builder;
using ORM.WebAPI.Infrastructure;

namespace ORM.WebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            //var corsAttr = new EnableCorsAttribute("*", "*", "*");
            //config.EnableCors(corsAttr);

            config.EnableCors();
            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.MapHttpAttributeRoutes();
            config.Formatters.Add(new HtmlGenerationTypeFormatter());
            //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            //builder.EntitySet<TypeOfTransactionModel>("Types");
            //config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
