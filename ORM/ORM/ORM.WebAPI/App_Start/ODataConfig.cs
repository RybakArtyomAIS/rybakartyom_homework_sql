﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using ORM.WebAPI.Models;
using ORM.BL.DTO.Models;

namespace ORM.WebAPI.App_Start
{
    public static class ODataConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // var corsAttr = new EnableCorsAttribute("*", "*", "*");
            //config.EnableCors(corsAttr);

            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            //config.MapHttpAttributeRoutes(); //This has to be called before the following OData mapping, so also before WebApi mapping

            //ODataModelBuilder builder = new ODataConventionModelBuilder();
            //builder.EntitySet<TypeOfTransactionModel>("TypeOfTransactionModels");

            //config.MapODataServiceRoute(
            //    routeName: "ODataRoute",
            //    routePrefix: null,
            //    model: builder.GetEdmModel());
        }
    }
}