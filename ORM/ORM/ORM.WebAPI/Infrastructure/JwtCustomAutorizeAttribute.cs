﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Net.Http;

namespace ORM.WebAPI.Infrastructure
{
    public class JwtCustomAutorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            string authHeader = null;
            var auth = actionContext.Request.Headers.Authorization;
            
            //if (auth != null && auth.Parameter != "undefined" && auth.Scheme == "Bearer")
            //    authHeader = auth.Parameter;

            //if(string.IsNullOrEmpty(authHeader))
            //{
            //    var query = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query);
            //    if (query.AllKeys.Contains("api_key"))
            //        authHeader = HttpUtility.UrlDecode(query["api_key"]);

            //    if (!string.IsNullOrEmpty(authHeader))
            //    {
            //        actionContext.RequestContext.Principal = JWT.JsonWebToken.DecodeToObject<QBUser>(authHeader, "Test");
            //    }
            //}
            var cookieHeader = actionContext.Request.Headers.GetCookies("api_key").FirstOrDefault();

            if (cookieHeader != null && cookieHeader.Cookies.Any())
            {
                var cookieVakue = cookieHeader.Cookies.First().Value;
                actionContext.RequestContext.Principal = JWT.JsonWebToken.DecodeToObject<QBUser>(cookieVakue, "Test");
            }
            base.OnAuthorization(actionContext);
        }
    }

    public class QBUser: IPrincipal
    {
        public string Name { get; set; }
        public List<string> Roles { get; set; }

        public IIdentity Identity
        {
            get { return new QBIdentity(Name); }
        }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }
    }

    public class QBIdentity: IIdentity
    {
        public QBIdentity(string name)
        {
            Name = name;
        }

        public string AuthenticationType
        {
            get { return "JWT"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string Name
        {
            get;
            private set;
        }
    }

    public static class ExtensionMethods
    {
        public static QBUser ToQBUser(this IPrincipal user)
        {
            if(user is QBUser)
            {
                return (QBUser)user;
            }
            return null;
        }
    }
}