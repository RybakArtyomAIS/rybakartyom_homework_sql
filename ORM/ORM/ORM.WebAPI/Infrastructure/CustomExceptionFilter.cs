﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace ORM.WebAPI.Infrastructure
{
    public class CustomExceptionFilter: ExceptionFilterAttribute
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            _logger.Error(actionExecutedContext.Exception, string.Format("Error from {0}.", actionExecutedContext.Request.RequestUri.AbsoluteUri));
            actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(actionExecutedContext.Exception.Message) };
            base.OnException(actionExecutedContext);
        }
    }
}