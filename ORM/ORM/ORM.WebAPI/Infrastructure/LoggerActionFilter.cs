﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace ORM.WebAPI.Infrastructure
{
    public class LoggerActionFilter : ActionFilterAttribute
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response.IsSuccessStatusCode)
            {
                _logger.Info(string.Format("Execute request for {0}", actionExecutedContext.Request.RequestUri.AbsoluteUri));
            }
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}