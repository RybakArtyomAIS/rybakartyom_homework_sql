﻿using ORM.BL.DTO.Models;
using ORM.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace ORM.WebAPI.Infrastructure
{
    public class HtmlGenerationTypeFormatter : BufferedMediaTypeFormatter
    {
        public HtmlGenerationTypeFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }

        public override bool CanReadType(Type type)
        {
            return false;
        }

        public override bool CanWriteType(Type type)
        {
            return true;
        }

        public override void WriteToStream(Type type, object value, Stream writeStream, HttpContent content)
        {
            using (var writer = new StreamWriter(writeStream))
            {
                var htmlTable = new StringBuilder();
                var typesOfTransaction = value as IEnumerable<TypeOfTransactionWebAPIModel>;
                if (typesOfTransaction != null)
                {
                    htmlTable.Append("<html><head></head><body><table><tr><th>ID</th><th>Name</th><th>Category</th></tr>");
                    foreach (var typeOfTransaction in typesOfTransaction)
                    {
                        htmlTable.AppendFormat("<tr><th>{0}</th><th>{1}</th><th>{2}</th></tr>", typeOfTransaction.Id, typeOfTransaction.Name, typeOfTransaction.CategoryOfTransactionId == 1 ? "Exp" : "Inc");
                    }
                    htmlTable.Append("</table></body></html>");
                }
                // content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                //content.Headers.ContentLength = htmlTable.Length;
                writer.Write(htmlTable.ToString());
            }

            //var writer = new StreamWriter(writeStream);
            //var typesOfTransaction = value as IEnumerable<TypeOfTransactionWebAPIModel>;
            //if (typesOfTransaction != null)
            //{
            //    foreach (var typeOfTransaction in typesOfTransaction)
            //    {
            //        typeOfTransaction.Name += ";";
            //        writer.WriteLine(string.Format("{0} {1} {2}", typeOfTransaction.Id, typeOfTransaction.Name, typeOfTransaction.CategoryOfTransactionId == 1 ? "Exp" : "Inc"));
            //    }
            //}
        } 
    }
}