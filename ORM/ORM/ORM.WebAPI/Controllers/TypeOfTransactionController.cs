﻿using ORM.BL.DTO;
using ORM.BL.DTO.Models;
using ORM.IoC;
using ORM.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ORM.WebAPI.Infrastructure;
using System.Net.Http.Headers;

namespace ORM.WebAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    [Authorize]
    public class TypeOfTransactionController : ApiController
    {
        private readonly IAccountService _accountService;
        
        public TypeOfTransactionController()
        {
            _accountService = IoContainer.GetIAccountServiceInstance;
        }

        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Login([FromBody]LoginWebAPIModel model)
        {
            var qbUser = new QBUser { Name = model.Login, Roles = new List<string>() { "RoleTest" } };
            var token = JWT.JsonWebToken.Encode(qbUser, "Test", JWT.JwtHashAlgorithm.HS256);
            //var resp = new HttpResponseMessage(HttpStatusCode.OK);
            //var cookie = new CookieHeaderValue("api_key", token);
            //cookie.Expires = DateTimeOffset.Now.AddMinutes(5);
            //cookie.Domain = Request.RequestUri.Host;

            //resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });

            //return resp;
            return Request.CreateResponse(HttpStatusCode.OK, token);
        }


        [HttpGet]
        [AllowAnonymous]
        public List<TypeOfTransactionWebAPIModel> GetAll()
        {
            var typeOfTransactionsList = _accountService.GetTypesOfTransaction().Select(t => t.ToWebAPI()).ToList();
            return typeOfTransactionsList;
            //return Request.CreateResponse(HttpStatusCode.OK, typeOfTransactionsList);
        }
        
        
        [HttpGet]
        public HttpResponseMessage GetById(int id)
        {
            var model = _accountService.GetTypeOfTransactionById(id);
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }

        
        [HttpPost]
        public HttpResponseMessage Create([FromBody]TypeOfTransactionWebAPIModel model)
        {
            var id = _accountService.CreateTypeOfTransaction(model.ToBL());
            return Request.CreateResponse(HttpStatusCode.OK, id);
        }

        
        [HttpPost]
        public HttpResponseMessage Update(TypeOfTransactionWebAPIModel model)
        {
            _accountService.UpdateTypeOfTransaction(model.ToBL());
            return Request.CreateResponse(HttpStatusCode.OK, "");
        }

        
        [HttpGet]
        public HttpResponseMessage Delete(int id)
        {
            _accountService.DropTypeOfTransaction(id);
            return Request.CreateResponse(HttpStatusCode.OK, id);
        }
    }

    
}
