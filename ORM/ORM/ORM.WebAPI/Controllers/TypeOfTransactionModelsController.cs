﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using ORM.BL.DTO.Models;
using Microsoft.Data.OData;
using ORM.BL.DTO;
using ORM.IoC;
using System.Web.Http.Cors;

namespace ORM.WebAPI.Controllers
{
    [EnableCors("*", "*", "*")]
    public class TypeOfTransactionModelsController : ApiController
    {
        private readonly IAccountService _accountService;

        public TypeOfTransactionModelsController()
        {
            _accountService = IoContainer.GetIAccountServiceInstance;
        }

        //[Route("Types")]
        [EnableQuery]
        [HttpGet]
        public IQueryable<TypeOfTransactionModel> Types()
        {
            var a = _accountService.TypesOfTransaction.Select(t => t.Name).ToList();
            return _accountService.TypesOfTransaction;
        }

        [Route("TestException")]
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage TestException()
        {
            var date = DateTime.UtcNow;
            throw new NotImplementedException(string.Format("Test exception. Date - {0} ", date));
        }
    }
}
