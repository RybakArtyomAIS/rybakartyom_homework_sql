﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.DAL
{
    public class CashFlowAdoModel
    {
        public string UsdBalance { get; set; }
        public string BlrBalance { get; set; }
        public string OldBlrBalance { get; set; }
        public string Date { get; set; }
    }
}
