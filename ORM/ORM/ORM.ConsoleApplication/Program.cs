﻿using System;
using System.Collections.Generic;
using System.Linq;
using ORM.BL.DTO.Models;
using ORM.IoC;
using ORM.BL.DTO;
using System.Text;

namespace ORM.ConsoleApplication
{
    class Program
    {
        private static IAccountService _accountService;
        static void Main(string[] args)
        {
            //Console.SetBufferSize(Console.BufferWidth, Int16.MaxValue - 1);
            //IoContainer.SetAdoConfig();
            //var accountServiceAdo = IoContainer.GetIAccountServiceInstance;
            //IoContainer.SetEFConfig();
            //var accountServiceEF = IoContainer.GetIAccountServiceInstance;
            //IoContainer.SetNhConfig();
            //var accountServiceNh = IoContainer.GetIAccountServiceInstance;
            //IoContainer.SetMongoConfig();
            //var accountServiceMongo = IoContainer.GetIAccountServiceInstance;

            //var accounts = accountServiceNh.GetAccounts();
            //foreach (var account in accounts)
            //{
            //    accountServiceMongo.SetAccount(account);
            //    Console.WriteLine("Set account to mongo ID {0}, Amount {1}, Count of transactions {2}", account.Id, account.Amount, account.TransactionList.Count());
            //}

            //var typeOfMoneyList = accountServiceNh.GetTypesOfMoney();
            //foreach (var type in typeOfMoneyList)
            //{
            //    accountServiceMongo.SetTypeOfMoney(type);
            //    Console.WriteLine("Set type of money to mongo ID {0}, Name {1}, Denomination {2}", type.Id, type.Name, type.DenominationList.Any() ? "Yes" : "No");
            //}
            //var cashFlowResults = new Dictionary<string, List<CashFlowModel>>();
            //cashFlowResults.Add("ADO.NET with execute sql view", accountServiceAdo.GetCashFlowView());
            //cashFlowResults.Add("Entity framework", accountServiceEF.GetCashFlowView());
            //cashFlowResults.Add("Fluent NHibernate", accountServiceNh.GetCashFlowView());
            ////cashFlowResults.Add("Mongo driver for C#", accountServiceMongo.GetCashFlowView());
            //Console.WriteLine("____________________________________");
            //foreach (var result in cashFlowResults)
            //{
            //    var lastItem = result.Value.First(item => item.Time != null);
            //    Console.WriteLine("{0}, Time {1}", result.Key, lastItem.Time);
            //    Console.WriteLine("Check: Date - {0}, Usd - {1}, BYN - {2}, BLR {3}, All count - {4}", lastItem.Date, lastItem.UsdBalance, lastItem.BlrBalance, lastItem.OldBlrBalance, result.Value.Count);
            //    Console.WriteLine();
            //}
            //var cashFlowView = accountServiceNh.GetCashFlowView();
            //foreach (var item in cashFlowView)
            //{
            //    Console.WriteLine("{0}, Usd {1}, Blr {2}, OldBlr {3}", item.Date, item.UsdBalance, item.BlrBalance, item.OldBlrBalance);
            //}

            IoContainer.SetEFConfig();
            _accountService = IoContainer.GetIAccountServiceInstance;

            var showMainMenuRange = new int[] { 0, 1, 2, 3, 4, 5 };
            ShowMainMenu();
            int userEnter = UserEnter(showMainMenuRange);
            while (userEnter != 0)
            {
                var result = ShowUserChoose(userEnter);
                if (result == "0")
                {
                    break;
                }
                if (result == "9")
                {
                    ShowMainMenu();
                }
                else
                {
                    ShowMainMenu(result);
                }
                userEnter = UserEnter(showMainMenuRange);
            }
        }

        private static string ShowUserChoose(int value)
        {
            var result = "";
            var showAllTransactionsMenuRange = new int[] { 0, 1, 2, 9 };
            var showAllCrossCoursesMenuRange = new int[] { 0, 1, 2, 9 };
            int userEnter;
            switch (value)
            {
                case 1:
                    //ShowAllAccountsBalancesMenu();
                    //int userEnter = UserEnter(showAllAccountsBalancesMenuRange);
                    result = ShowAllAccountsBalancesResult();
                    break;
                case 2:
                    ShowAllTransactionsMenu();
                    userEnter = UserEnter(showAllTransactionsMenuRange);
                    result = ShowAllTransactionsResult(userEnter);
                    break;
                case 3:
                    ShowAllCrossCoursesMenu();
                    userEnter = UserEnter(showAllCrossCoursesMenuRange);
                    result = ShowAllCrossCoursesResult(userEnter);
                    break;
                case 4:
                    result = CreateTransactionResult();
                    break;
                case 5:
                    result = UpdateAccountBalanceResult();
                    break;
                default:
                    break;
            }
            return result;
        }

        private static string CreateTransactionResult()
        {
            Console.WriteLine("Choose account and currency:");
            var typeOfMoneyName = ShowAndChooseItemsForUser(_accountService.GetTypesOfMoney().Select(t => t.Name));
            Console.WriteLine("Entering transaction's date:");
            var dateTime = UserEnterDate();
            Console.WriteLine("Choose type of transaction:");
            var typeOfTransaction = ShowAndChooseItemsForUser(_accountService.GetTypesOfTransaction().Select(t => t.Name));
            Console.WriteLine("Entering transaction's amount in selecting currency:");
            var amount = GetAmountFromUser();
            var id = _accountService.CreateTransaction(typeOfMoneyName, dateTime, amount < 0 ? -1 * amount : amount, typeOfMoneyName, amount < 0 ? "Exp" : "Inc");
            return string.Format("Transaction created successfully. Id - {0}", id);
        }

        private static decimal GetAmountFromUser()
        {
            var enterStr = Console.ReadLine().Trim();
            decimal enterValue;
            while (!Decimal.TryParse(enterStr, out enterValue) || enterValue == 0)
            {
                Console.WriteLine("Enter wrong amount from transaction!");
                enterStr = Console.ReadLine().Trim();
            }
            return enterValue;
        }

        private static string ShowAndChooseItemsForUser(IEnumerable<string> items)
        {
            var showItemsDict = new Dictionary<int, string>();
            var index = 1;
            foreach (var itemName in items)
            {
                showItemsDict.Add(index, itemName);
                Console.WriteLine("{0}. {1}", index, itemName);
                index++;
            }
            var userEnter = UserEnter(showItemsDict.Keys.ToArray());
            return showItemsDict[userEnter];
        }

        private static string UpdateAccountBalanceResult()
        {
            var accountIds = _accountService.GetAccounts().Select(a => a.Id);
            foreach (var accounId in accountIds)
            {
                _accountService.UpdateAccountBalance(accounId);
            }
            return "Update accounts balance executed successfully.";
        }

        private static string ShowAllCrossCoursesResult(int userEnter)
        {
            var resultStrBuld = new StringBuilder();
            switch(userEnter)
            {
                case 1:
                    var allCrossCourses = _accountService.GetAllCrossCourses();
                    Console.WriteLine("Entering date:");
                    var userEnterByDate = UserEnterDate(allCrossCourses.Select(c => c.Date));
                    var typeOfMoneyNames = _accountService.GetTypesOfMoney().Select(t => t.Name);
                    foreach(var typeMoneyOfName in typeOfMoneyNames)
                    {
                        var crossCourseModel = _accountService.GetCrossCourseByDate(userEnterByDate, typeMoneyOfName);
                        if (crossCourseModel != null)
                        {
                            resultStrBuld.AppendLine(string.Format("{0}, 1 {1} -> {2} {3}",
                               crossCourseModel.Date, crossCourseModel.FirstTypeOfMoneyName, 
                               crossCourseModel.Difference, crossCourseModel.SecondTypeOfMoneyName));
                        }
                    }
                    break;
                case 2:
                    var crossCourses = _accountService.GetAllCrossCourses().OrderBy(c => c.Date);
                    foreach(var crossCourseModel in crossCourses)
                    {
                        resultStrBuld.AppendLine(string.Format("{0}, {1} -> {2} :  {3} {4}", 
                            crossCourseModel.Date, crossCourseModel.FirstTypeOfMoneyName, 
                            crossCourseModel.SecondTypeOfMoneyName, crossCourseModel.Difference, crossCourseModel.FirstTypeOfMoneyName));
                    }
                    break;
                case 9:
                case 0:
                default:
                    resultStrBuld.Append(userEnter);
                    break;
            }
            return resultStrBuld.ToString();
        }

        private static DateTime UserEnterDate(IEnumerable<DateTime> range = null)
        {
            var enterStr = Console.ReadLine().Trim();
            DateTime enterValue;
            while ((!DateTime.TryParse(enterStr, out enterValue)) || (range != null && !range.ToList().Contains(enterValue)))
            {
                Console.WriteLine("Enter wrong date!");
                enterStr = Console.ReadLine().Trim();
            }
            return enterValue;
        }

        private static void ShowAllCrossCoursesMenu()
        {
            Console.Clear();
            Console.WriteLine("1. Show cross courses by date. ");
            Console.WriteLine("2. Show all cross courses. ");
            Console.WriteLine("9. Back to main menu. ");
            Console.WriteLine("0. Exit program. ");
        }

        private static string ShowAllTransactionsResult(int userEnter)
        {
            var resultStrBuld = new StringBuilder();
            switch(userEnter)
            {
                case 1:
                    var transactions = _accountService.GetTransactions().OrderBy(t => t.Date);
                    Console.WriteLine("Entering transaction's id:");
                    var userEnterById = UserEnter(transactions.Select(t => t.Id).ToArray());
                    var transactionModel = _accountService.GetTransactionById(userEnterById);
                    resultStrBuld.AppendLine(string.Format("{0}, {1} {2} {3}, type - {4}", 
                            transactionModel.Date, transactionModel.CategoryName == "Inc" ? "+" : "-", 
                            transactionModel.Total, transactionModel.TypeOfMoneyName, transactionModel.TypeName));
                    break;
                case 2:
                    var allTransactions = _accountService.GetTransactions().OrderBy(t => t.Date);
                    foreach(var transaction in allTransactions)
                    {
                        resultStrBuld.AppendLine(string.Format("{0}, {1} {2} {3}, type - {4}",
                            transaction.Date, transaction.CategoryName == "Inc" ? "+" : "-",
                            transaction.Total, transaction.TypeOfMoneyName, transaction.TypeName));
                    }
                    break;
                case 9:
                case 0:
                default:
                    resultStrBuld.Append(userEnter);
                    break;
            }
            return resultStrBuld.ToString();
        }

        private static void ShowAllTransactionsMenu()
        {
            Console.Clear();
            Console.WriteLine("1. Show transaction by id. ");
            Console.WriteLine("2. Show all transactions. ");
            Console.WriteLine("9. Back to main menu. ");
            Console.WriteLine("0. Exit program. ");
        }

        private static string ShowAllAccountsBalancesResult()
        {
            var accounts = _accountService.GetAccounts();
            var accountsStringBuilder = new StringBuilder();
            foreach (var accountModel in accounts)
            {
                accountsStringBuilder.AppendLine(string.Format("Id - {0}, Account's currency: {1}, Amount - {2}", accountModel.Id, accountModel.TypeOfMoneyName, accountModel.Amount));
            }
            return accountsStringBuilder.ToString();
        }

        private static int UserEnter(int[] range)
        {
            var enterStr = Console.ReadLine().Trim();
            int enterValue;
            while ((!Int32.TryParse(enterStr, out enterValue)) || (!range.Contains(enterValue)))
            {
                Console.WriteLine("Enter wrong number!");
                enterStr = Console.ReadLine().Trim();
            }
            return enterValue;
        }

        private static void ShowMainMenu(string result = "")
        {
            Console.Clear();
            if (!String.IsNullOrWhiteSpace(result))
            {
                Console.WriteLine(result);
                Console.WriteLine("_________________");
            }
            Console.WriteLine("1. Show account's balances. ");
            Console.WriteLine("2. Show transactions. ");
            Console.WriteLine("3. Show cross courses. ");
            Console.WriteLine("4. Create transaction. ");
            Console.WriteLine("5. Update account balance. ");
            Console.WriteLine("0. Exit. ");
        }
    }
}
