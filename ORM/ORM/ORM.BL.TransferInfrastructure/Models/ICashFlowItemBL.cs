﻿using System;

namespace ORM.BL.TransferInfrastructure.Models
{
    public interface ICashFlowItemBL
    {
        DateTime Date { get; set; }
        decimal UsdBalance { get; set; }
        decimal BlrBalance { get; set; }
        decimal OldBlrBalance { get; set; }
    }
}
