﻿using System;

namespace ORM.BL.DTO.Models
{
    public class TransactionModel
    {
        public int Id { get; set; }
        public string TypeOfMoneyName { get; set; }
        public decimal Total { get; set; }
        public DateTime Date { get; set; }

        public int TypeOfTransactionId { get; set; }

        public int AccountId { get; set; }
        public string TypeName { get; set; }
        public string CategoryName { get; set; }
    }
}
