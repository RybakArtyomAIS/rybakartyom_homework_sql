﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.BL.DTO.Models
{
    public class AccountModel
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int TypeOfMoneyId { get; set; }
        public string TypeOfMoneyName { get; set; }
        public List<TransactionModel> TransactionList { get; set; }
    }
}
