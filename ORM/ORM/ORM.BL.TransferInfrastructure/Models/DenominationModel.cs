﻿namespace ORM.BL.DTO.Models
{
    public class DenominationModel
    {
        public int Id { get; set; }

        public decimal DifferenceToOne { get; set; }

        public int TypeOfMoneyModelId { get; set; }
    }
}
