﻿using System.Collections.Generic;
using System.Linq;
using ORM.DAL.DTO.Models;

namespace ORM.BL.DTO.Models
{
    public static class ExtensionsMethods
    {
        public static AccountDAL ToDAL(this AccountModel model)
        {
            var result = new AccountDAL
            {
                Id = model.Id,
                Amount = model.Amount,
                TypeOfMoneyId = model.TypeOfMoneyId,
                TransactionsList = model.TransactionList != null ? model.TransactionList.Select(ToDAL).ToList() : new List<TransactionDAL>(),
                TypeOfMoneyName = model.TypeOfMoneyName
            };
            return result;
        }

        public static AccountModel ToModel(this AccountDAL dal)
        {
            var result = new AccountModel
            {
                Id = dal.Id,
                Amount = dal.Amount,
                TypeOfMoneyId = dal.TypeOfMoneyId,
                TransactionList = dal.TransactionsList != null ? dal.TransactionsList.Select(ToModel).ToList() : new List<TransactionModel>(),
                TypeOfMoneyName = dal.TypeOfMoneyName
            };
            return result;
        }

        public static TransactionDAL ToDAL(this TransactionModel model)
        {
            return new TransactionDAL
            {
                Id = model.Id,
                Date = model.Date,
                Total = model.Total,
                AccountId = model.AccountId,
                TypeOfTransactionId = model.TypeOfTransactionId,
                CategoryName = model.CategoryName,
                TypeName = model.TypeName,
                TypeOfMoneyName = model.TypeOfMoneyName
            };
        }

        public static TransactionModel ToModel(this TransactionDAL dal)
        {
            return new TransactionModel
            {
                Id = dal.Id,
                Date = dal.Date,
                Total = dal.Total,
                AccountId = dal.AccountId,
                TypeOfTransactionId = dal.TypeOfTransactionId,
                CategoryName = dal.CategoryName,
                TypeName = dal.TypeName,
                TypeOfMoneyName = dal.TypeOfMoneyName
            };
        }


        public static TypeOfTransactionDAL ToDAL(this TypeOfTransactionModel model)
        {
            return new TypeOfTransactionDAL
            {
                Id = model.Id,
                Name = model.Name,
                CategoryOfTransactionId = model.CategoryOfTransactionId
            };
        }

        public static TypeOfTransactionModel ToModel(this TypeOfTransactionDAL dal)
        {
            return new TypeOfTransactionModel
            {
                Id = dal.Id,
                Name = dal.Name,
                CategoryOfTransactionId = dal.CategoryOfTransactionId
            };
        }


        public static CategoryOfTransactionDAL ToDAL(this CategoryOfTransactionModel model)
        {
            return new CategoryOfTransactionDAL
            {
                Id = model.Id,
                Name = model.Name
            };
        }

        public static CategoryOfTransactionModel ToModel(this CategoryOfTransactionDAL dal)
        {
            return new CategoryOfTransactionModel
            {
                Id = dal.Id,
                Name = dal.Name
            };
        }


        public static TypeOfMoneyDAL ToDAL(this TypeOfMoneyModel model)
        {
            var result = new TypeOfMoneyDAL
            {
                Id = model.Id,
                Name = model.Name,
                CrossCourseList = model.CrossCourseList != null ? model.CrossCourseList.Select(ToDAL).ToList() : new List<CrossCourseDAL>(),
                DenominationList = model.DenominationList != null ? model.DenominationList.Select(ToDAL).ToList() : new List<DenominationDAL>()
            };
            return result;
        }

        public static TypeOfMoneyModel ToModel(this TypeOfMoneyDAL dal)
        {
            var result = new TypeOfMoneyModel
            {
                Id = dal.Id,
                Name = dal.Name,
                CrossCourseList = dal.CrossCourseList != null ? dal.CrossCourseList.Select(ToModel).ToList() : new List<CrossCourseModel>(),
                DenominationList = dal.DenominationList != null ? dal.DenominationList.Select(ToModel).ToList() : new List<DenominationModel>()
            };
            return result;
        }


        public static CrossCourseDAL ToDAL(this CrossCourseModel model)
        {
            return new CrossCourseDAL
            {
                Id = model.Id,
                Difference = model.Difference,
                Date = model.Date,
                FirstTypeOfMoneyId = model.FirstTypeOfMoneyModelId, 
                SecondTypeOfMoneyId = model.SecondTypeOfMoneyId, 
                SecondTypeOfMoneyName = model.SecondTypeOfMoneyName
            };
        }

        public static CrossCourseModel ToModel(this CrossCourseDAL dal)
        {
            return new CrossCourseModel
            {
                Id = dal.Id,
                Difference = dal.Difference,
                Date = dal.Date,
                FirstTypeOfMoneyModelId = dal.FirstTypeOfMoneyId,
                FirstTypeOfMoneyName = dal.FirstTypeOfMoneyName,
                SecondTypeOfMoneyId = dal.SecondTypeOfMoneyId, 
                SecondTypeOfMoneyName = dal.SecondTypeOfMoneyName
            };
        }


        public static DenominationDAL ToDAL(this DenominationModel model)
        {
            return new DenominationDAL
            {
                Id = model.Id,
                DifferenceToOne = model.DifferenceToOne,
                TypeOfMoneyId = model.TypeOfMoneyModelId
            };
        }

        public static DenominationModel ToModel(this DenominationDAL dal)
        {
            return new DenominationModel
            {
                Id = dal.Id,
                DifferenceToOne = dal.DifferenceToOne,
                TypeOfMoneyModelId = dal.TypeOfMoneyId
            };
        }

        public static CashFlowItemDAL ToDAL(this CashFlowModel model)
        {
            return new CashFlowItemDAL
            {
                Date = model.Date,
                UsdBalance = model.UsdBalance,
                BlrBalance = model.BlrBalance,
                OldBlrBalance = model.OldBlrBalance,
                Time = model.Time
            };
        }

        public static CashFlowModel ToModel(this CashFlowItemDAL dal)
        {
            return new CashFlowModel
            {
                Date = dal.Date,
                UsdBalance = dal.UsdBalance,
                BlrBalance = dal.BlrBalance,
                OldBlrBalance = dal.OldBlrBalance,
                Time = dal.Time
            };
        }

    }
}
