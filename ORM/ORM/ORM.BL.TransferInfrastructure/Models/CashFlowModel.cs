﻿using System;

namespace ORM.BL.DTO.Models
{
    public class CashFlowModel
    {
        public decimal UsdBalance { get; set; }
        public decimal BlrBalance { get; set; }
        public decimal OldBlrBalance { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan? Time { get; set; }
    }
}
