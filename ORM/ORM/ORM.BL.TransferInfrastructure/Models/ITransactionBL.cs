﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORM.BL.TransferInfrastructure.Models
{
    public interface ITransactionBL
    {
        int Id { get; set; }
        DateTime Date { get; set; }
        decimal Total { get; set; }
        ITypeOfTransactionBL TypeOfTransaction { get; set; }
        IAccountBL Account { get; set; }
    }
}
