﻿namespace ORM.BL.DTO.Models
{
    public class TypeOfTransactionModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int CategoryOfTransactionId { get; set; }
    }
}
