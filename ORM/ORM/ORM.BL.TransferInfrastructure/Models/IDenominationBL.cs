﻿namespace ORM.BL.TransferInfrastructure.Models
{
    public interface IDenominationBL
    {
        int Id { get; set; }
        double DifferenceToOne { get; set; }
        ITypeOfMoneyBL TypeOfMoney { get; set; }
    }
}