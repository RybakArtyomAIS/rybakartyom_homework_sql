﻿namespace ORM.BL.DTO.Models
{
    public class CategoryOfTransactionModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
