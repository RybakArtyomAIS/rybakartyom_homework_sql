﻿using System;

namespace ORM.BL.DTO.Models
{
    public class CrossCourseModel
    {
        public int Id { get; set; }
        public double Difference { get; set; }
        public DateTime Date { get; set; }

        public int FirstTypeOfMoneyModelId { get; set; }
        public string FirstTypeOfMoneyName { get; set; }

        public int SecondTypeOfMoneyId { get; set; }

        public string SecondTypeOfMoneyName { get; set; }

    }
}
