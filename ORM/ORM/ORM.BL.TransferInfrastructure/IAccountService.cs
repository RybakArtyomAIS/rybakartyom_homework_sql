﻿using System.Collections.Generic;
using ORM.BL.DTO.Models;
using System;
using System.Linq;

namespace ORM.BL.DTO
{
    public interface IAccountService
    {
        List<AccountModel> GetAccounts();
        void SetAccount(AccountModel account);
        List<TypeOfMoneyModel> GetTypesOfMoney();
        void SetTypeOfMoney(TypeOfMoneyModel typeOfMoney);

        List<CashFlowModel> GetCashFlowView();

        int CreateTypeOfMoney(string name);
        void DropTypeOfMoney(int id);
        AccountModel ShowAccountBalance(int id);

        CrossCourseModel GetCrossCourseByDate(DateTime date, string typeOfMoneyName);

        void UpdateCrossCourse(int id, double difference);

        List<TransactionModel> GetTransactions();

        int CreateTransaction(string typeOfMoneyName, DateTime date, decimal amount, string type, string category);

        void DropTransaction(int id);

        void UpdateTransaction(int p, TransactionModel transactionModel);

        TransactionModel GetTransactionById(int id);


        void UpdateAccountBalance(int accountId);

        List<CrossCourseModel> GetAllCrossCourses();

        
        TypeOfTransactionModel GetTypeOfTransactionById(int id);

        List<TypeOfTransactionModel> GetTypesOfTransaction(); 
        
        int CreateTypeOfTransaction(TypeOfTransactionModel model);

        void UpdateTypeOfTransaction(TypeOfTransactionModel model);

        void DropTypeOfTransaction(int id);

        IQueryable<TypeOfTransactionModel> TypesOfTransaction { get; set; }
    }
}
