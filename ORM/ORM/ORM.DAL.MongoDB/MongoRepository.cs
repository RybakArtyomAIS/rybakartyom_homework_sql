﻿using MongoDB.Driver;
using System.Linq;
using System.Configuration;
using ORM.DAL.MongoDB.Models;
using System.Threading.Tasks;
using ORM.DAL.DTO;
using ORM.DAL.DTO.Models;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using System.Diagnostics;

namespace ORM.DAL.MongoDB
{
    public class MongoRepository : IRepository
    {
        private readonly IMongoDatabase _database;
        public MongoRepository()
        {
            var client = new MongoClient(ConfigurationManager.ConnectionStrings["MongoDb"].ConnectionString);
            _database = client.GetDatabase("Training_Rybak");
            DropCollection("Accounts");
            DropCollection("TypesOfMoney");
        }

        public void SetAccount(AccountDAL account)
        {
            AddAccount(account.ToMongo());
        }

        private void AddAccount(AccountMongo account)
        {
            var accountList = _database.GetCollection<AccountMongo>("Accounts");
            accountList.InsertOne(account);
        }

        public void SetTypeOfMoney(TypeOfMoneyDAL typeOfMoney)
        {
            AddTypeOfMoney(typeOfMoney.ToMongo());
        }

        public List<CashFlowItemDAL> GetCashFlowView()
        {
            var result = new List<CashFlowItemDAL>();
            var timer = new Stopwatch();
            timer.Start();
            var typeOfMoneyCollection = _database.GetCollection<TypeOfMoneyMongo>("TypesOfMoney");
            var accountCollection = _database.GetCollection<AccountMongo>("Accounts");
            //var filter = Builders<BsonDocument>.Filter.Eq("TypesOfMoney", "Usd");
            //var test = typeOfMoneyCollection.Find(filter).ToList();
            //var crossCourses = typeOfMoneyCollection.Find(new BsonDocument()).First().CrossCourses.OrderBy(c => c.Date);
            var brTransactions = accountCollection.Find(a => a.TypeOfMoney == "Br").First().Transactions.ToList();
            var usdTransactions = accountCollection.Find(a => a.TypeOfMoney == "Usd").First().Transactions.ToList();

            var currentTime = typeOfMoneyCollection.Find(new BsonDocument()).First().CrossCourses.OrderBy(c => c.Date).First().Date;
            var endTime = typeOfMoneyCollection.Find(new BsonDocument()).First().CrossCourses.OrderByDescending(c => c.Date).First().Date;
            decimal usdBalance = 0;
            decimal blrBalance = 0;
            var denomination = typeOfMoneyCollection.Find(t => t.Name == "Br").First().Denominations.First().DifferenceToOne;
            while (currentTime <= endTime)
            {
                //var usdCurrentTransactions = accountCollection.Find(a => a.TypeOfMoney == "Usd").First().Transactions.Where(item => item.Date == currentTime).ToList();
                //var blrCurrentTransactions = accountCollection.Find(a => a.TypeOfMoney == "Br").First().Transactions.Where(item => item.Date == currentTime).ToList();
                usdBalance += usdTransactions.Where(item => item.Type == "Inc").Sum(item => item.Amount);
                usdBalance -= usdTransactions.Where(item => item.Type == "Exp").Sum(item => item.Amount);
                blrBalance += brTransactions.Where(item => item.Type == "Inc").Sum(item => item.Amount);
                blrBalance -= brTransactions.Where(item => item.Type == "Exp").Sum(item => item.Amount);
                result.Add(new CashFlowItemDAL
                {
                    Date = currentTime,
                    BlrBalance = blrBalance,
                    UsdBalance = usdBalance,
                    OldBlrBalance = blrBalance * denomination
                });
                currentTime = currentTime.AddDays(1);
            }
            timer.Stop();
            result.OrderByDescending(item => item.Date).First().Time = timer.Elapsed;
            return result;
        }

        private void AddTypeOfMoney(TypeOfMoneyMongo typeOfMoney)
        {
            var accountList = _database.GetCollection<TypeOfMoneyMongo>("TypesOfMoney");
            accountList.InsertOne(typeOfMoney);
        }

        private void CreateIfNotExists(string collectionName)
        {
            var collection = _database.GetCollection<BsonDocument>(collectionName);
            var filter = new BsonDocument();
            if (collection.Count(filter) == 0)
            {
                _database.CreateCollection(collectionName);
            }
        }

        private void DropCollection(string name)
        {
            var collection = _database.GetCollection<BsonDocument>(name);
            var filter = new BsonDocument();
            collection.DeleteMany(filter);
            //CreateIfNotExists(name);
        }

        #region Contains of throw new System.NotImplementedException()


        public void UpdateAccountBalance(int accountId)
        {
            throw new System.NotImplementedException();
        }

        public List<CrossCourseDAL> GetCrossCourses()
        {
            throw new System.NotImplementedException();
        }

        public List<TypeOfMoneyDAL> GetTypesOfMoney()
        {
            throw new System.NotImplementedException();
        }

        public List<TransactionDAL> GetTransactions(int? accountId = null)
        {
            throw new System.NotImplementedException();
        }

        public List<TypeOfTransactionDAL> GetTypesTransaction()
        {
            throw new System.NotImplementedException();
        }

        public List<AccountDAL> GetAccounts()
        {
            throw new System.NotImplementedException();
        }

        public int CreateTypeOfMoney(TypeOfMoneyDAL typeOfMoneyDAL)
        {
            throw new System.NotImplementedException();
        }

        public void DropTypeOfMoney(int id)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateCrossCourse(int id, CrossCourseDAL crossCourse)
        {
            throw new System.NotImplementedException();
        }

        public int CreateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            throw new System.NotImplementedException();
        }

        public void DropTransaction(int id)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateTransaction(int id, TransactionDAL transactionDAL)
        {
            throw new System.NotImplementedException();
        }

        public int CreateTransactions(TransactionDAL transactionDAL)
        {
            throw new System.NotImplementedException();
        }

        public List<CategoryOfTransactionDAL> GetCategoriesOfTransaction()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            throw new System.NotImplementedException();
        }

        public void DropTypeOfTransaction(int id)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<TypeOfTransactionDAL> TypesOfTransaction
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }
        #endregion
    }
}
