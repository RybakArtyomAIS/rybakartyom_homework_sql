﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ORM.DAL.MongoDB.Models
{
    public class AccountMongo
    {
        [BsonId]
        public int Id { get; set; }
        public decimal Total { get; set; }
        public int TypeOfMoneyId { get; set; }
        public string TypeOfMoney { get; set; }
        public List<TransactionMongo> Transactions { get; set; }
    }
}
