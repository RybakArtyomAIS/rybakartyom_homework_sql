﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace ORM.DAL.MongoDB.Models
{
    public class TypeOfMoneyMongo
    {
        [BsonId]
        public int Id { get; set; }
        public string Name { get; set; }
        public List<DenominationMongo> Denominations { get; set; }
        public List<CrossCourseMongo> CrossCourses { get; set; }
    }
}
