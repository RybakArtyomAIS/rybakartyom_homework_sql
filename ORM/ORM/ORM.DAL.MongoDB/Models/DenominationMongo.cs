﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORM.DAL.MongoDB.Models
{
    public class DenominationMongo
    {
        public int Id { get; set; }
        public decimal DifferenceToOne { get; set; }
    }
}
