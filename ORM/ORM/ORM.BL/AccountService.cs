﻿using System.Collections.Generic;
using System.Linq;
using ORM.BL.DTO;
using ORM.BL.DTO.Models;
using ORM.DAL.DTO;

namespace ORM.BL
{
    public class AccountService : IAccountService
    {
        private readonly IRepository _repository;

        public AccountService(IRepository repository)
        {
            _repository = repository;
        }

        public List<AccountModel> GetAccounts()
        {
            var result = new List<AccountModel>();
            var accounts = _repository.GetAccounts();
            result.AddRange(accounts.Select(item => item.ToModel()));
            return result;
        }

        public List<TypeOfMoneyModel> GetTypesOfMoney()
        {
            var result = new List<TypeOfMoneyModel>();
            var types = _repository.GetTypesOfMoney();
            result.AddRange(types.Select(item => item.ToModel()));
            return result;
        }

        public List<CashFlowModel> GetCashFlowView()
        {
            var result = new List<CashFlowModel>();
            var view = _repository.GetCashFlowView();
            result.AddRange(view.Select(item => item.ToModel()));
            return result;
        }

        public void SetAccount(AccountModel account)
        {
            _repository.SetAccount(account.ToDAL());
        }

        public void SetTypeOfMoney(TypeOfMoneyModel typeOfMoney)
        {
            _repository.SetTypeOfMoney(typeOfMoney.ToDAL());
        }


        public int CreateTypeOfMoney(string name)
        {
            return _repository.CreateTypeOfMoney(new TypeOfMoneyModel { Name = name }.ToDAL());
        }

        public void DropTypeOfMoney(int id)
        {
            _repository.DropTypeOfMoney(id);
        }

        public AccountModel ShowAccountBalance(int id)
        {
            var accounts = _repository.GetAccounts().Where(a => a.Id == id);
            return accounts.Any() ? accounts.First().ToModel() : null;
        }

        public CrossCourseModel GetCrossCourseByDate(System.DateTime date, string typeOfMoneyName)
        {
            var crossCourses = _repository.GetCrossCourses().Where(c => c.Date == date && c.SecondTypeOfMoneyName == typeOfMoneyName);
            return crossCourses.Any() ? crossCourses.First().ToModel() : null;
        }

        public void UpdateCrossCourse(int id, double difference)
        {
            var crossCourse = _repository.GetCrossCourses().FirstOrDefault(c => c.Id == id);
            crossCourse.Difference = difference;
            if (crossCourse != null)
            {
                _repository.UpdateCrossCourse(id, crossCourse);
            }
        }

        public List<TransactionModel> GetTransactions()
        {
            return _repository.GetTransactions().Select(t => t.ToModel()).ToList();
        }

        public int CreateTransaction(string typeOfMoneyName, System.DateTime date, decimal amount, string type, string category)
        {
            var account = _repository.GetAccounts().First(a => a.TypeOfMoneyName == typeOfMoneyName).ToModel();
            TypeOfTransactionModel typeOfTransaction;
            var categoryOfTransaction = _repository.GetCategoriesOfTransaction().First(c => c.Name == category).ToModel();
            if (!_repository.GetTypesTransaction().Any(t => t.Name == type && t.CategoryOfTransactionId == categoryOfTransaction.Id))
            {
                typeOfTransaction = new TypeOfTransactionModel
                {
                    Name = type,
                    CategoryOfTransactionId = categoryOfTransaction.Id
                };
                typeOfTransaction.Id = _repository.CreateTypeOfTransaction(typeOfTransaction.ToDAL());
            }
            else
            {
                typeOfTransaction = _repository.GetTypesTransaction().First(t => t.Name == type).ToModel();
            }
            return _repository.CreateTransactions(new TransactionModel 
            { 
                Date = date, 
                Total = amount, 
                AccountId = account.Id, 
                TypeOfTransactionId = typeOfTransaction.Id 
            }.ToDAL());
        }

        public void DropTransaction(int id)
        {
            _repository.DropTransaction(id);
        }

        public void UpdateTransaction(int id, TransactionModel transactionModel)
        {
            _repository.UpdateTransaction(id, transactionModel.ToDAL());
        }

        public TransactionModel GetTransactionById(int id)
        {
            var transaction = _repository.GetTransactions().FirstOrDefault(t => t.Id == id);
            return transaction != null ? transaction.ToModel() : null;
        }

        public TypeOfTransactionModel GetTypeOfTransactionById(int id)
        {
            var typeOfTransaction = _repository.GetTypesTransaction().FirstOrDefault(t => t.Id == id);
            return typeOfTransaction != null ? typeOfTransaction.ToModel() : null;
        }


        public void UpdateAccountBalance(int accountId)
        {
            _repository.UpdateAccountBalance(accountId);
        }


        public List<CrossCourseModel> GetAllCrossCourses()
        {
            return _repository.GetCrossCourses().Select(c => c.ToModel()).ToList();
        }


        public List<TypeOfTransactionModel> GetTypesOfTransaction()
        {
            return _repository.GetTypesTransaction().Select(t => t.ToModel()).ToList();
        }


        public int CreateTypeOfTransaction(TypeOfTransactionModel model)
        {
            return _repository.CreateTypeOfTransaction(model.ToDAL());
        }


        public void UpdateTypeOfTransaction(TypeOfTransactionModel model)
        {
            _repository.UpdateTypeOfTransaction(model.ToDAL());
        }

        public void DropTypeOfTransaction(int id)
        {
            _repository.DropTypeOfTransaction(id);
        }


        public IQueryable<TypeOfTransactionModel> TypesOfTransaction
        {
            get
            {
                return _repository.TypesOfTransaction.Select(t => new TypeOfTransactionModel
                {
                    Id = t.Id,
                    Name = t.Name,
                    CategoryOfTransactionId = t.CategoryOfTransactionId
                });
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
