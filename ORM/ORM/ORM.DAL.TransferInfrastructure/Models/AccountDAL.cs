﻿using System.Collections.Generic;

namespace ORM.DAL.DTO.Models
{
    public class AccountDAL
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public int TypeOfMoneyId { get; set; }

        public string TypeOfMoneyName { get; set; }

        public List<TransactionDAL> TransactionsList { get; set; }
    }
}
