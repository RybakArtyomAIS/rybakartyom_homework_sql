﻿using System;

namespace ORM.DAL.DTO.Models
{
    public class CashFlowItemDAL
    {
        public DateTime Date { get; set; }

        public decimal UsdBalance { get; set; }

        public decimal BlrBalance { get; set; }

        public decimal OldBlrBalance { get; set; }

        public TimeSpan? Time { get; set; }
    }
}
