﻿namespace ORM.DAL.DTO.Models
{
    public class TypeOfTransactionDAL
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public int CategoryOfTransactionId { get; set; }
    }
}
