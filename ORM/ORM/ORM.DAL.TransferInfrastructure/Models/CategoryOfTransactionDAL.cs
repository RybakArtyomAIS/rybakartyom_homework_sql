﻿namespace ORM.DAL.DTO.Models
{
    public class CategoryOfTransactionDAL
    {

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
