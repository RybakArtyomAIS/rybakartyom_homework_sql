﻿using System;
namespace ORM.DAL.DTO.Models
{
    public class CrossCourseDAL
    {
        public int Id { get; set; }

        public double Difference { get; set; }

        public DateTime Date { get; set; }

        public int FirstTypeOfMoneyId { get; set; }

        public string FirstTypeOfMoneyName { get; set; }

        public int SecondTypeOfMoneyId { get; set; }

        public string SecondTypeOfMoneyName { get; set; }
    }
}