﻿using System.Collections.Generic;

namespace ORM.DAL.DTO.Models
{
    public class TypeOfMoneyDAL
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<DenominationDAL> DenominationList { get; set; }

        public List<CrossCourseDAL> CrossCourseList { get; set; }
    }
}
