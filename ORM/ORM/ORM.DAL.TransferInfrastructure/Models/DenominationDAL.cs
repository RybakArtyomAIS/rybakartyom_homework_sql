﻿namespace ORM.DAL.DTO.Models
{
    public class DenominationDAL
    {
        public int Id { get; set; }

        public decimal DifferenceToOne { get; set; }

        public int TypeOfMoneyId { get; set; }
    }
}