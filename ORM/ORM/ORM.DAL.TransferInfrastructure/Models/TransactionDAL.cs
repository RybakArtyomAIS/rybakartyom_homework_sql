﻿using System;

namespace ORM.DAL.DTO.Models
{
    public class TransactionDAL
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public decimal Total { get; set; }

        public int TypeOfTransactionId { get; set; }

        public int AccountId { get; set; }

        public string TypeName { get; set; }

        public string CategoryName { get; set; }

        public string TypeOfMoneyName { get; set; }
    }
}
