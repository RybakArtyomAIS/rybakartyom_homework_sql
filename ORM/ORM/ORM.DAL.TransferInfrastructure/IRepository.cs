﻿using System.Collections.Generic;
using ORM.DAL.DTO.Models;
using System.Linq;

namespace ORM.DAL.DTO
{
    public interface IRepository
    {
        List<CrossCourseDAL> GetCrossCourses();
        //void SetCrossCourse(CrossCourseDAL course);

        //List<DenominationDAL> GetDenomination();
        //void SetDenomination(DenominationDAL denomination);

        List<TypeOfMoneyDAL> GetTypesOfMoney();
        void SetTypeOfMoney(TypeOfMoneyDAL typeOfMoney);
        
        List<TransactionDAL> GetTransactions(int? accountId = null);
        //void SetTransaction(TransactionDAL transaction);

        List<TypeOfTransactionDAL> GetTypesTransaction();
        //void SetTypeOfTransaction(TypeOfTransactionDAL transaction);

        //List<CategoryOfTransactionDAL> GetCategoriesOfTransaction();
        //void SetCategoryOfTransaction(CategoryOfTransactionDAL categoryOfTransaction);

        List<AccountDAL> GetAccounts();

        void SetAccount(AccountDAL account);

        List<CashFlowItemDAL> GetCashFlowView();

        int CreateTypeOfMoney(TypeOfMoneyDAL typeOfMoneyDAL);

        void DropTypeOfMoney(int id);

        void UpdateCrossCourse(int id, CrossCourseDAL crossCourse);

        int CreateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL);

        void DropTransaction(int id);

        void UpdateTransaction(int id, TransactionDAL transactionDAL);

        int CreateTransactions(TransactionDAL transactionDAL);

        List<CategoryOfTransactionDAL> GetCategoriesOfTransaction();

        void UpdateAccountBalance(int accountId);

        void UpdateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL);

        void DropTypeOfTransaction(int id);

        IQueryable<TypeOfTransactionDAL> TypesOfTransaction { get; set; }
    }
}
