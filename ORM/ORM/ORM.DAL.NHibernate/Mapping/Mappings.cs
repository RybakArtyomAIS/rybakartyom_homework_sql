﻿using FluentNHibernate.Mapping;
using ORM.DAL.NHibernate.Entities;

namespace ORM.DAL.NHibernate.Mapping
{
    public class AccountMap : ClassMap<AccountNh>
    {
        public AccountMap()
        {
            Table("Accounts");
            
            Id(x => x.Id);
            Map(x => x.Total);
            Map(x => x.TypeOfMoneyId);
            References(x => x.TypeOfMoney).Column("TypeOfMoneyId");
            HasMany(x => x.TransactionsList).KeyColumn("AccountId").Inverse().Cascade.All();
        }
    }

    public class TypeOfMoneyMap : ClassMap<TypeOfMoneyNh>
    {
        public TypeOfMoneyMap()
        {
            Table("TypesOfMoney");
            
            Id(x => x.Id);
            Map(x => x.Name);
            HasMany(x => x.Accounts).KeyColumn("TypeOfMoneyId").Inverse().Cascade.All();
            HasMany(x => x.Denominations).KeyColumn("TypeId").Inverse().Cascade.All();
            HasMany(x => x.CrossCourses).KeyColumn("FirstTypeId").Inverse().Cascade.All();
            HasMany(x => x.CrossCoursesSecond).KeyColumn("SecondTypeId").Inverse().Cascade.All();
        }
    }

    public class DenominationMap : ClassMap<DenominationNh>
    {
        public DenominationMap()
        {
            Table("Denominations");
            
            Id(x => x.Id);
            Map(x => x.DifferenceToOne);
            Map(x => x.TypeId);
            References(x => x.TypeOfMoney).Column("TypeId");
        }
    }

    public class CrossCourseMap : ClassMap<CrossCourseNh>
    {
        public CrossCourseMap()
        {
            Table("CrossCourses");
            
            Id(x => x.Id);
            Map(x => x.Difference);
            Map(x => x.CourseDate);
            Map(x => x.FirstTypeId);
            Map(x => x.SecondTypeId);
            References(x => x.FirstTypeOfMoney).Column("FirstTypeId");
            References(x => x.SecondTypeOfMoney).Column("SecondTypeId");
        }
    }

    public class TransactionMap : ClassMap<TransactionNh>
    {
        public TransactionMap()
        {
            Table("Transactions");
            
            Id(x => x.Id);
            Map(x => x.CreateDate);
            Map(x => x.AccountId);
            Map(x => x.Amount);
            Map(x => x.TypeId);
            References(x => x.Account).Column("AccountId");
            References(x => x.TypeOfTransaction).Column("TypeId");
        }
    }

    public class TypeOfTransactionMap : ClassMap<TypeOfTransactionNh>
    {
        public TypeOfTransactionMap()
        {
            Table("TypesOfTransaction");
            
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.CategoryId);
            References(x => x.Category).Column("CategoryId");
            HasMany(x => x.Transactions).KeyColumn("TypeId").Inverse().Cascade.All();
        }
    }

    public class CategoryMap : ClassMap<CategoryOfTransactionNh>
    {
        public CategoryMap()
        {
            Table("CategoriesOfTransaction");
            
            Id(x => x.Id);
            Map(x => x.Name);
            HasMany(x => x.Types).KeyColumn("CategoryId").Inverse().Cascade.All();
        }
    }
}
