﻿using System.Collections.Generic;

namespace ORM.DAL.NHibernate.Entities
{
    public class AccountNh
    {
        public virtual int Id { get; set; }

        public virtual decimal Total { get; set; }

        public virtual TypeOfMoneyNh TypeOfMoney { get; set; }

        public virtual int TypeOfMoneyId { get; set; }

        public virtual IList<TransactionNh> TransactionsList { get; set; }

        public AccountNh()
        {
            TransactionsList = new List<TransactionNh>();
        }
    }
}
