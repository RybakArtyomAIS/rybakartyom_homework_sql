﻿using System;

namespace ORM.DAL.NHibernate.Entities
{
    public class CashFlowItemNh
    {
        public virtual DateTime Date { get; set; }

        public virtual decimal UsdBalance { get; set; }

        public virtual decimal BlrBalance { get; set; }

        public virtual decimal OldBlrBalance { get; set; }
    }
}
