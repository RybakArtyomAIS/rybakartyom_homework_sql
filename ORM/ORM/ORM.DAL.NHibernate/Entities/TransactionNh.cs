﻿using System;

namespace ORM.DAL.NHibernate.Entities
{

    public class TransactionNh
    {
        public virtual int Id { get; set; }

        public virtual DateTime CreateDate { get; set; }

        public virtual decimal Amount { get; set; }

        public virtual TypeOfTransactionNh TypeOfTransaction { get; set; }

        public virtual int TypeId { get; set; }

        public virtual int AccountId { get; set; }

        public virtual AccountNh Account { get; set; }
    }
}
