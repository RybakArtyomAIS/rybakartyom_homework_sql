﻿using System.Collections.Generic;

namespace ORM.DAL.NHibernate.Entities
{
    public class TypeOfMoneyNh
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual IList<DenominationNh> Denominations { get; set; }

        public virtual IList<CrossCourseNh> CrossCourses { get; set; }

        public virtual IList<CrossCourseNh> CrossCoursesSecond { get; set; }

        public virtual IList<AccountNh> Accounts { get; set; }

        public TypeOfMoneyNh()
        {
            Denominations = new List<DenominationNh>();
            CrossCourses = new List<CrossCourseNh>();
            CrossCoursesSecond = new List<CrossCourseNh>();
            Accounts = new List<AccountNh>();
        }
    }
}
