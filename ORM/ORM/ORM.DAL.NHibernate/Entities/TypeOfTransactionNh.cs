﻿using System.Collections.Generic;

namespace ORM.DAL.NHibernate.Entities
{

    public class TypeOfTransactionNh
    {

        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual CategoryOfTransactionNh Category { get; set; }

        public virtual int CategoryId { get; set; }

        public virtual IList<TransactionNh> Transactions { get; set; }

        public TypeOfTransactionNh()
        {
            Transactions = new List<TransactionNh>();
        }
    }
}
