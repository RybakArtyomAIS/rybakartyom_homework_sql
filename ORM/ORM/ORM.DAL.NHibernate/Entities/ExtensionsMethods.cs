﻿using System;
using System.Linq;
using ORM.DAL.DTO.Models;

namespace ORM.DAL.NHibernate.Entities
{
    public static class ExtensionsMethods
    {
        public static AccountDAL ToDAL(this AccountNh entity)
        {
            var result = new AccountDAL
            {
                Id = entity.Id,
                Amount = entity.Total,
                TypeOfMoneyId = entity.TypeOfMoneyId,
                TransactionsList = entity.TransactionsList.Select(ToDAL).ToList(),
                TypeOfMoneyName = entity.TypeOfMoney != null ? entity.TypeOfMoney.Name : ""
            };
            return result;
        }

        public static AccountNh ToNhibernate(this AccountDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
            //var result = new AccountNh
            //{
            //    Id = dal.Id,
            //    Amount = dal.Amount,
            //    TypeOfMoney = dal.TypeOfMoney.ToNhibernate()
            //};
            //result.TransactionList = dal.TransactionsList.Select(item => new TransactionNh
            //{
            //    Id = item.Id,
            //    Date = item.Date,
            //    Total = item.Total,
            //    AccountNh = result,
            //    TypeOfTransactionNh = item.TypeOfTransaction.ToNhibernate()
            //}).ToList();
            //return result;
        }

        public static TransactionDAL ToDAL(this TransactionNh entity)
        {
            return new TransactionDAL
            {
                Id = entity.Id,
                Date = entity.CreateDate,
                Total = entity.Amount,
                AccountId = entity.AccountId,
                TypeOfTransactionId = entity.TypeId,
                CategoryName = entity.TypeOfTransaction != null && entity.TypeOfTransaction.Category != null ? entity.TypeOfTransaction.Category.Name : "",
                TypeName = entity.TypeOfTransaction != null ? entity.TypeOfTransaction.Name : ""
            };
        }

        public static TransactionNh ToNhibernate(this TransactionDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
            //return new TransactionNh
            //{
            //    Id = dal.Id,
            //    Date = dal.Date,
            //    Total = dal.Total, 
            //    AccountNh = dal.Account.ToNhibernate(),
            //    TypeOfTransactionNh = dal.TypeOfTransaction.ToNhibernate()
            //};
        }


        public static TypeOfTransactionDAL ToDAL(this TypeOfTransactionNh entity)
        {
            return new TypeOfTransactionDAL
            {
                Id = entity.Id,
                Name = entity.Name,
                CategoryOfTransactionId = entity.CategoryId
            };
        }

        public static TypeOfTransactionNh ToNhibernate(this TypeOfTransactionDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
            //return new TypeOfTransactionNh
            //{
            //    Id = dal.Id,
            //    Name = dal.Name,
            //    CategoryOfTransactionNh = dal.CategoryOfTransaction.ToNhibernate()
            //};
        }


        public static CategoryOfTransactionDAL ToDAL(this CategoryOfTransactionNh entity)
        {
            return new CategoryOfTransactionDAL
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public static CategoryOfTransactionNh ToNhibernate(this CategoryOfTransactionDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
            //return new CategoryOfTransactionNh
            //{
            //    Id = dal.Id,
            //    Name = dal.Name
            //};
        }


        public static TypeOfMoneyDAL ToDAL(this TypeOfMoneyNh entity)
        {
            var result = new TypeOfMoneyDAL
            {
                Id = entity.Id,
                Name = entity.Name,
                CrossCourseList = entity.CrossCourses.Select(ToDAL).ToList(),
                DenominationList = entity.Denominations.Select(ToDAL).ToList()
            };
            return result;
        }

        public static TypeOfMoneyNh ToNhibernate(this TypeOfMoneyDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
            //var result = new TypeOfMoneyNh
            //{
            //    Id = dal.Id,
            //    Name = dal.Name
            //};
            //result.CrossCourseList = dal.CrossCourseList.Select(item => new CrossCourseNh
            //{
            //    Id = item.Id,
            //    Difference = item.Difference,
            //    Date = item.Date,
            //    FirstTypeOfMoneyNh = result, 
            //    SecondTypeOfMoneyId = item.SecondTypeOfMoneyId, 
            //    SecondTypeOfMoneyName = item.SecondTypeOfMoneyName
            //}).ToList();
            //result.DenominationList = dal.DenominationList.Select(item => new DenominationNh
            //{
            //    Id = item.Id,
            //    DifferenceToOne = item.DifferenceToOne,
            //    TypeOfMoneyNh = result
            //}).ToList();
            //return result;
        }


        public static CrossCourseDAL ToDAL(this CrossCourseNh entity)
        {
            return new CrossCourseDAL
            {
                Id = entity.Id,
                Difference = entity.Difference,
                Date = entity.CourseDate,
                FirstTypeOfMoneyId = entity.FirstTypeId,
                SecondTypeOfMoneyId = entity.SecondTypeId,
                SecondTypeOfMoneyName = entity.SecondTypeOfMoney.Name
            };
        }

        public static CrossCourseNh ToNhibernate(this CrossCourseDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
            //return new CrossCourseNh
            //{
            //    Id = dal.Id,
            //    Difference = dal.Difference,
            //    Date = dal.Date,
            //    FirstTypeOfMoneyNh = dal.FirstTypeOfMoney.ToNhibernate(), 
            //    SecondTypeOfMoneyId = dal.SecondTypeOfMoneyId, 
            //    SecondTypeOfMoneyName = dal.SecondTypeOfMoneyName
            //};
        }


        public static DenominationDAL ToDAL(this DenominationNh entity)
        {
            return new DenominationDAL
            {
                Id = entity.Id,
                DifferenceToOne = entity.DifferenceToOne,
                TypeOfMoneyId = entity.TypeId
            };
        }

        public static DenominationNh ToNhibernate(this DenominationDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
            //return new DenominationNh
            //{
            //    Id = dal.Id,
            //    DifferenceToOne = dal.DifferenceToOne,
            //    TypeOfMoneyNh = dal.TypeOfMoney.ToNhibernate()
            //};
        }

        public static CashFlowItemDAL ToDAL(this CashFlowItemNh entity)
        {
            return new CashFlowItemDAL
            {
                Date = entity.Date,
                UsdBalance = entity.UsdBalance,
                BlrBalance = entity.BlrBalance,
                OldBlrBalance = entity.OldBlrBalance
            };
        }

        public static CashFlowItemNh ToNhibernate(this CashFlowItemDAL dal)
        {
            throw new Exception("Extension method DAL to Nh doesn't make");
        //    return new CashFlowItemNh
        //    {
        //        Date = dal.Date,
        //        UsdBalance = dal.UsdBalance,
        //        BlrBalance = dal.BlrBalance,
        //        OldBlrBalance = dal.OldBlrBalance
        //    };
        }

    }
}
