﻿using System;
namespace ORM.DAL.NHibernate.Entities
{

    public class CrossCourseNh
    {
        public virtual int Id { get; set; }

        public virtual double Difference { get; set; }

        public virtual DateTime CourseDate { get; set; }

        public virtual TypeOfMoneyNh SecondTypeOfMoney { get; set; }

        public virtual TypeOfMoneyNh FirstTypeOfMoney { get; set; }

        public virtual int FirstTypeId { get; set; }

        public virtual int SecondTypeId { get; set; }
    }
}