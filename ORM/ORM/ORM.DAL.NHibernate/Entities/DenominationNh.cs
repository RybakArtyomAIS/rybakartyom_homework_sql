﻿namespace ORM.DAL.NHibernate.Entities
{
    public class DenominationNh
    {
        public virtual int Id { get; set; }

        public virtual decimal DifferenceToOne { get; set; }

        public virtual TypeOfMoneyNh TypeOfMoney { get; set; }

        public virtual int TypeId { get; set; }
    }
}