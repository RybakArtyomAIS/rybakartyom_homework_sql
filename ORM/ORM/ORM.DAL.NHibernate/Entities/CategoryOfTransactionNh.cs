﻿using System.Collections.Generic;

namespace ORM.DAL.NHibernate.Entities
{
    public class CategoryOfTransactionNh
    {

        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual IList<TypeOfTransactionNh> Types { get; set; }

        public CategoryOfTransactionNh()
        {
            Types = new List<TypeOfTransactionNh>();
        }
    }
}
