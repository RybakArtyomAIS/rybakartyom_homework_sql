﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using FluentNHibernate.Cfg;
using FluentNHibernate.Conventions;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Transform;
using ORM.DAL.DTO;
using ORM.DAL.DTO.Models;
using ORM.DAL.NHibernate.Entities;

namespace ORM.DAL.NHibernate
{
    public class NhRepository : IRepository
    {
        private static ISessionFactory GetSession()
        {
            return Fluently.Configure()
                    .Database(
                        MsSqlConfiguration.MsSql2012.ConnectionString(
                            ConfigurationManager.ConnectionStrings["AccountDb"].ConnectionString).ShowSql())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<TypeOfMoneyNh>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<AccountNh>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<CrossCourseNh>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<DenominationNh>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<TransactionNh>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<TypeOfTransactionNh>())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<CategoryOfTransactionNh>())
                    .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, false))
                    .BuildSessionFactory();
        }

        public List<TypeOfMoneyDAL> GetTypesOfMoney()
        {
            var result = new List<TypeOfMoneyDAL>();
            using (var session = GetSession().OpenSession())
            {
                var typeOfMoneyList = session.Query<TypeOfMoneyNh>().ToList();
                //typeOfMoneyList.ForEach(t =>
                //{
                //    t.Denominations = session.Query<DenominationNh>().Where(d => d.TypeId == t.Id).ToList();
                //    t.CrossCourses = session.Query<CrossCourseNh>().Where(c => c.FirstTypeId == t.Id).ToList();
                //    t.CrossCoursesSecond = session.Query<CrossCourseNh>().Where(c => c.SecondTypeId == t.Id).ToList();
                //    t.CrossCourses.ToList().ForEach(c => c.FirstTypeOfMoney = t);
                //    t.CrossCoursesSecond.ToList().ForEach(c => c.SecondTypeOfMoney = t);
                //    t.Denominations.ToList().ForEach(d => d.TypeOfMoney = t);
                //});
                typeOfMoneyList.ForEach(item => result.Add(item.ToDAL()));
            }
            return result;
        }

        public List<AccountDAL> GetAccounts()
        {
            var result = new List<AccountDAL>();
            using (var session = GetSession().OpenSession())
            {
                var accountList =
                    session.Query<AccountNh>().ToList();
                accountList.ForEach(a =>
                {
                    a.TypeOfMoney = session.Get<TypeOfMoneyNh>(a.TypeOfMoneyId);
                    //a.TransactionsList = session.Query<TransactionNh>().Where(t => t.AccountId == a.Id).ToList();
                    //a.TransactionsList.ToList().ForEach(t =>
                    //{
                    //    t.TypeOfTransaction = session.Get<TypeOfTransactionNh>(t.TypeId);
                    //    t.TypeOfTransaction.Category =
                    //        session.Get<CategoryOfTransactionNh>(t.TypeOfTransaction.CategoryId);
                    //    t.Account = a;
                    //});
                });
                accountList.ForEach(item => result.Add(item.ToDAL()));
            }
            return result;
        }

        public List<CashFlowItemDAL> GetCashFlowView()
        {
            var result = new List<CashFlowItemDAL>();
            var timer = new Stopwatch();
            using (var session = GetSession().OpenSession())
            {
                timer.Start();
                var currentDate = session.Query<CrossCourseNh>().OrderBy(item => item.CourseDate).First().CourseDate;
                var endDate = session.Query<CrossCourseNh>().OrderByDescending(item => item.CourseDate).First().CourseDate;
                decimal usdBalance = 0;
                decimal blrBalance = 0;
                var denomination = session.Query<DenominationNh>().ToList().First(d => d.TypeId == session.Query<TypeOfMoneyNh>().First(t => t.Name == "Br").Id).DifferenceToOne;
                var dateList = new List<DateTime>();
                while (currentDate <= endDate)
                {
                    dateList.Add(currentDate);
                    currentDate = currentDate.AddDays(1);
                }
                result = session.Query<TransactionNh>().GroupBy(t => t.CreateDate).ToList().Select(g => new CashFlowItemDAL
                {
                    Date = g.Key,
                    UsdBalance = g.Count(t => t.Account.TypeOfMoney.Name == "Usd") > 0 ? g.Where(t => t.Account.TypeOfMoney.Name == "Usd").Sum(t => t.TypeOfTransaction.Category.Name == "Inc" ? t.Amount : t.Amount * (-1)) : 0,
                    BlrBalance = g.Count(t => t.Account.TypeOfMoney.Name == "Br") > 0 ? g.Where(t => t.Account.TypeOfMoney.Name == "Br").Sum(t => t.TypeOfTransaction.Category.Name == "Inc" ? t.Amount : t.Amount * (-1)) : 0
                })
                .Union(dateList.Select(d => new CashFlowItemDAL { Date = d, UsdBalance = 0, BlrBalance = 0 }))
                .GroupBy(c => c.Date)
                .OrderBy(g => g.Key)
                .Select(g =>
                {
                    usdBalance += g.Sum(c => c.UsdBalance);
                    blrBalance += g.Sum(c => c.BlrBalance);
                    return new CashFlowItemDAL
                    {
                        Date = g.Key.Date,
                        UsdBalance = usdBalance,
                        BlrBalance = blrBalance,
                        OldBlrBalance = blrBalance * denomination
                    };
                }).ToList();
                //while (currentDate <= endDate)
                //{
                //    var transactionsList = session.Query<TransactionNh>().Where(t => t.CreateDate == currentDate).ToList();
                //    transactionsList.ForEach(t =>
                //    {
                //        t.TypeOfTransaction = session.Get<TypeOfTransactionNh>(t.TypeId);
                //        t.TypeOfTransaction.Category = session.Get<CategoryOfTransactionNh>(t.TypeOfTransaction.CategoryId);
                //        t.Account = session.Get<AccountNh>(t.AccountId);
                //        t.Account.TypeOfMoney = session.Get<TypeOfMoneyNh>(t.Account.TypeOfMoneyId);
                //    });
                //    usdBalance += transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Usd" && t.TypeOfTransaction.Category.Name == "Inc").ToList().Sum(t => t.Amount);
                //    usdBalance -= transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Usd" && t.TypeOfTransaction.Category.Name == "Exp").ToList().Sum(t => t.Amount);
                //    blrBalance += transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Br" && t.TypeOfTransaction.Category.Name == "Inc").ToList().Sum(t => t.Amount);
                //    blrBalance -= transactionsList.Where(t => t.Account.TypeOfMoney.Name == "Br" && t.TypeOfTransaction.Category.Name == "Exp").ToList().Sum(t => t.Amount);
                //    result.Add(new CashFlowItemDAL
                //    {
                //        Date = currentDate,
                //        BlrBalance = blrBalance,
                //        OldBlrBalance = blrBalance * denomination,
                //        UsdBalance = usdBalance
                //    });
                //    currentDate = currentDate.AddDays(1);
                //}
                timer.Stop();
            }
            result.OrderByDescending(item => item.Date).First().Time = timer.Elapsed;
            return result;
        }
        #region throw new NotImplementedException
        
        public List<CategoryOfTransactionDAL> GetCategoriesOfTransaction()
        {
            throw new NotImplementedException();
        }

        public IQueryable<TypeOfTransactionDAL> TypesOfTransaction
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void UpdateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            throw new NotImplementedException();
        }

        public void DropTypeOfTransaction(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateAccountBalance(int accountId)
        {
            throw new NotImplementedException();
        }

        public List<CrossCourseDAL> GetCrossCourses()
        {
            throw new NotImplementedException();
        }

        public void SetTypeOfMoney(TypeOfMoneyDAL typeOfMoney)
        {
            throw new NotImplementedException();
        }

        public List<TransactionDAL> GetTransactions(int? accountId = null)
        {
            throw new NotImplementedException();
        }

        public List<TypeOfTransactionDAL> GetTypesTransaction()
        {
            throw new NotImplementedException();
        }

        public void SetAccount(AccountDAL account)
        {
            throw new NotImplementedException();
        }

        public int CreateTypeOfMoney(TypeOfMoneyDAL typeOfMoneyDAL)
        {
            throw new NotImplementedException();
        }

        public void DropTypeOfMoney(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateCrossCourse(int id, CrossCourseDAL crossCourse)
        {
            throw new NotImplementedException();
        }

        public int CreateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            throw new NotImplementedException();
        }

        public void DropTransaction(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateTransaction(int id, TransactionDAL transactionDAL)
        {
            throw new NotImplementedException();
        }

        public int CreateTransactions(TransactionDAL transactionDAL)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
