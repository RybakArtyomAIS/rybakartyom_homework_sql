﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Diagnostics;
using ORM.DAL.DTO;
using ORM.DAL.DTO.Models;

namespace ORM.DAL.ADONET
{
    public class AdoRepository : IRepository
    {
        private readonly string _dataProvider;
        private readonly string _connectionString;

        public AdoRepository()
        {
            _dataProvider = ConfigurationManager.AppSettings["provider"];
            _connectionString = ConfigurationManager.AppSettings["cnStr"];
        }


        public List<CashFlowItemDAL> GetCashFlowView()
        {
            var result = new List<CashFlowItemDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);
            var timer = new Stopwatch();
            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC ExecuteCashFlowView";
                timer.Start();
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new CashFlowItemDAL
                        {
                            BlrBalance = (Decimal)dr["BlrBalance"],
                            Date = (DateTime)dr["Date"],
                            OldBlrBalance = (Decimal)dr["OldBlrBalance"],
                            UsdBalance = (Decimal)dr["UsdBalance"]
                        });
                    }
                }
                timer.Stop();
            }
            result.OrderByDescending(item => item.Date).First().Time = timer.Elapsed;
            return result;
        }

        public List<CrossCourseDAL> GetCrossCourses()
        {
            var result = new List<CrossCourseDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetCrossCourses " + 0;
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new CrossCourseDAL
                        {
                            Id = (int)dr["Id"],
                            Date = (DateTime)dr["Date"],
                            Difference = (double)dr["Difference"],
                            SecondTypeOfMoneyId = (int)dr["SecondId"],
                            SecondTypeOfMoneyName = dr["SecondName"].ToString()
                        });
                    }
                }
            }

            return result;
        }

        public List<TransactionDAL> GetTransactions(int? accountId)
        {
            var result = new List<TransactionDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetAccountTransactions " + Convert.ToString(accountId ?? -1);
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new TransactionDAL
                        {
                            Id = (int)dr["Id"],
                            Date = (DateTime)dr["CreateDate"],
                            Total = (Decimal)dr["Amount"]
                            //TypeOfTransaction = new TypeOfTransactionDAL
                            //{
                            //    Id = (int)dr["TypeID"],
                            //    Name = dr["TypeName"].ToString(),
                            //    CategoryOfTransaction = new CategoryOfTransactionDAL
                            //    {
                            //        Id = (int)dr["CategoryID"],
                            //        Name = dr["CategoryName"].ToString()
                            //    }
                            //}
                        });
                    }
                }
            }

            return result;
        }

        public List<AccountDAL> GetAccounts()
        {
            var result = new List<AccountDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetAccounts";
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new AccountDAL
                        {
                            Id = (int)dr["Id"],
                            Amount = (decimal)dr["Total"]
                            //TypeOfMoney = new TypeOfMoneyDAL
                            //{
                            //    Id = (int)dr["TypeOfMoneyId"],
                            //    Name = dr["TypeOfMoneyName"].ToString(),
                            //    CrossCourseList = new List<CrossCourseDAL>(),
                            //    DenominationList = new List<DenominationDAL>()
                            //}
                        });
                    }
                }

                foreach (var account in result)
                {
                    var transactionList = new List<TransactionDAL>();
                    cmd = df.CreateCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "EXEC GetAccountTransactions " + account.Id;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            transactionList.Add(new TransactionDAL
                            {
                                Id = (int)dr["Id"],
                                Date = (DateTime)dr["CreateDate"],
                                Total = (Decimal)dr["Amount"]
                                //TypeOfTransaction = new TypeOfTransactionDAL
                                //{
                                //    Id = (int)dr["TypeID"],
                                //    Name = dr["TypeName"].ToString(),
                                //    CategoryOfTransaction = new CategoryOfTransactionDAL
                                //    {
                                //        Id = (int)dr["CategoryID"],
                                //        Name = dr["CategoryName"].ToString()
                                //    }
                                //},
                                //Account = account
                            });
                        }
                    }
                    account.TransactionsList = transactionList;
                }
            }

            return result;
        }

        public List<DTO.Models.TypeOfMoneyDAL> GetTypesOfMoney()
        {
            var result = new List<TypeOfMoneyDAL>();
            var df = DbProviderFactories.GetFactory(_dataProvider);

            using (var cn = df.CreateConnection())
            {
                cn.ConnectionString = _connectionString;
                cn.Open();

                var cmd = df.CreateCommand();
                cmd.Connection = cn;
                cmd.CommandText = "EXEC GetTypesOfMoney";
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add(new TypeOfMoneyDAL
                        {
                            Id = (int)dr["Id"],
                            Name = dr["Name"].ToString()
                        });
                    }
                }

                foreach (var typeOfMoney in result)
                {
                    var crossCourseList = new List<CrossCourseDAL>();
                    var denominationList = new List<DenominationDAL>();
                    cmd = df.CreateCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "EXEC GetCrossCourses " + typeOfMoney.Id;
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            crossCourseList.Add(new CrossCourseDAL
                            {
                                Id = (int)dr["Id"],
                                Date = (DateTime)dr["Date"],
                                Difference = (double)dr["Difference"], 
                                SecondTypeOfMoneyId = (int)dr["SecondId"], 
                                SecondTypeOfMoneyName = dr["SecondName"].ToString()
                            });
                        }
                    }
                    typeOfMoney.CrossCourseList = crossCourseList;

                    cmd = df.CreateCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "EXEC GetDenomination " + typeOfMoney.Id;
                    using (var dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                            while (dr.Read())
                            {
                                denominationList.Add(new DenominationDAL
                                {
                                    Id = (int)dr["Id"],
                                    DifferenceToOne = (decimal)dr["DifferenceToOne"],
                                    //TypeOfMoney = typeOfMoney
                                });
                            }
                    }
                    typeOfMoney.DenominationList = denominationList;
                }
            }
            return result;
        }

        #region Contains of throw new System.NotImplementedException();

        public List<CategoryOfTransactionDAL> GetCategoriesOfTransaction()
        {
            throw new NotImplementedException();
        }

        public void UpdateAccountBalance(int accountId)
        {
            throw new NotImplementedException();
        }

        public void SetTypeOfMoney(TypeOfMoneyDAL typeOfMoney)
        {
            throw new NotImplementedException();
        }

        public List<TypeOfTransactionDAL> GetTypesTransaction()
        {
            throw new NotImplementedException();
        }

        public void SetAccount(AccountDAL account)
        {
            throw new NotImplementedException();
        }

        public int CreateTypeOfMoney(TypeOfMoneyDAL typeOfMoneyDAL)
        {
            throw new NotImplementedException();
        }

        public void DropTypeOfMoney(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateCrossCourse(int id, CrossCourseDAL crossCourse)
        {
            throw new NotImplementedException();
        }

        public int CreateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            throw new NotImplementedException();
        }

        public void DropTransaction(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateTransaction(int id, TransactionDAL transactionDAL)
        {
            throw new NotImplementedException();
        }

        public int CreateTransactions(TransactionDAL transactionDAL)
        {
            throw new NotImplementedException();
        }

        public void UpdateTypeOfTransaction(TypeOfTransactionDAL typeOfTransactionDAL)
        {
            throw new NotImplementedException();
        }

        public void DropTypeOfTransaction(int id)
        {
            throw new NotImplementedException();
        }


        public IQueryable<TypeOfTransactionDAL> TypesOfTransaction
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}
