USE TRAINING
GO

IF OBJECT_ID('dbo.DepartmentMyEmployees') IS NOT NULL
  DROP TABLE DepartmentMyEmployees
GO

IF OBJECT_ID('dbo.ProjectMyEmployees') IS NOT NULL
  DROP TABLE ProjectMyEmployees
GO

IF OBJECT_ID('dbo.SPLIT') IS NOT NULL
  DROP FUNCTION SPLIT
GO

IF OBJECT_ID('dbo.Departments') IS NOT NULL
  DROP TABLE Departments
GO

IF OBJECT_ID('dbo.Projects') IS NOT NULL
  DROP TABLE Projects
GO

IF OBJECT_ID('dbo.MyEmployees') IS NOT NULL
  DROP TABLE MyEmployees
GO

IF OBJECT_ID('dbo.Countries') IS NOT NULL
  DROP TABLE Countries
GO


CREATE FUNCTION [dbo].[Split]
(
    @String NVARCHAR(4000),
    @Delimiter NCHAR(1)
)
RETURNS TABLE
AS
RETURN
(
    WITH Split(stpos,endpos)
    AS(
        SELECT 0 AS stpos, CHARINDEX(@Delimiter,@String) AS endpos
        UNION ALL
        SELECT endpos+1, CHARINDEX(@Delimiter,@String,endpos+1)
            FROM Split
            WHERE endpos > 0
    )
    SELECT 'Id' = ROW_NUMBER() OVER (ORDER BY (SELECT 1)),
        'Data' = SUBSTRING(@String,stpos,COALESCE(NULLIF(endpos,0),LEN(@String)+1)-stpos)
    FROM Split
)
GO
CREATE TABLE Departments
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
)

CREATE TABLE Projects
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
)

CREATE TABLE Countries
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
)

CREATE TABLE MyEmployees
(
	Id int NOT NULL PRIMARY KEY IDENTITY(1, 1),
	Name nvarchar(250) NOT NULL,
	Position nvarchar(250) NOT NULL,
	CountryId int NOT NULL FOREIGN KEY REFERENCES Countries(Id)
)

CREATE TABLE DepartmentMyEmployees
(
	DepartmentId int NOT NULL,
	EmployeeId int NOT NULL,
	CONSTRAINT PK_DepartmentMyEmployees PRIMARY KEY (DepartmentId, EmployeeId),
	CONSTRAINT FK_Department
    FOREIGN KEY (DepartmentId) REFERENCES Departments (Id),
	CONSTRAINT FK_MyEmployee1
    FOREIGN KEY (EmployeeId) REFERENCES MyEmployees (Id)
)

CREATE TABLE ProjectMyEmployees
(
	ProjectId int NOT NULL,
	EmployeeId int NOT NULL,
	CONSTRAINT PK_ProjectMyEmployees PRIMARY KEY (ProjectId, EmployeeId),
	CONSTRAINT FK_Project
    FOREIGN KEY (ProjectId) REFERENCES Projects (Id),
	CONSTRAINT FK_MyEmployee
    FOREIGN KEY (EmployeeId) REFERENCES MyEmployees (Id)
)

GO

INSERT INTO Countries 
(Name)
SELECT E.Country 
FROM Employees E
GROUP BY E.Country 

GO

INSERT INTO MyEmployees 
(Name, Position, CountryId)
SELECT E.Name, E.Position, C.Id as CountryId 
FROM Employees E
INNER JOIN Countries C ON C.Name = E.Country
GO

INSERT INTO Projects
(Name)
SELECT RTRIM(LTRIM(Data))
FROM Employees E CROSS APPLY dbo.Split(E.Project, ',')
WHERE DATA != '*'
GROUP BY RTRIM(LTRIM(Data))
GO

INSERT INTO Departments
(Name)
SELECT RTRIM(LTRIM(Data))
FROM Employees E CROSS APPLY dbo.Split(E.Department, ',')
WHERE Data != '* Departments'
GROUP BY RTRIM(LTRIM(Data))
GO


INSERT INTO ProjectMyEmployees
(EmployeeId, ProjectId)
SELECT MyE.Id AS EmployeeId, P.Id AS ProjectId
FROM Employees E 
INNER JOIN MyEmployees MyE ON E.Name = MyE.Name
INNER JOIN Projects P ON CHARINDEX(P.Name, E.Project) > 0

INSERT INTO ProjectMyEmployees
(EmployeeId, ProjectId)
SELECT MyE.Id AS EmployeeId, P.Id AS ProjectId
FROM Employees E 
INNER JOIN MyEmployees MyE ON E.Name = MyE.Name
INNER JOIN Projects P ON E.Project = '*'
GO

INSERT INTO DepartmentMyEmployees
(EmployeeId, DepartmentId)
SELECT MyE.Id AS EmployeeId, D.Id AS DepartmentId
FROM Employees E 
INNER JOIN MyEmployees MyE ON E.Name = MyE.Name
INNER JOIN Departments D ON CHARINDEX(D.Name, E.Department) > 0

INSERT INTO DepartmentMyEmployees
(EmployeeId, DepartmentId)
SELECT MyE.Id AS EmployeeId, D.Id AS DepartmentId
FROM Employees E 
INNER JOIN MyEmployees MyE ON E.Name = MyE.Name
INNER JOIN Departments D ON E.Department = '* Departments'
GO

SELECT E.NAME, E.POSITION, C.NAME AS COUNTRY, D.NAME AS DEPARTMENT, P.NAME AS PROJECT
FROM dbo.MyEmployees E
INNER JOIN dbo.Countries C ON E.CountryId = C.Id
INNER JOIN dbo.DepartmentMyEmployees [DE] ON [DE].EmployeeId = E.Id
INNER JOIN dbo.Departments D ON [DE].DepartmentId = D.Id
INNER JOIN dbo.ProjectMyEmployees [PE] ON [PE].EmployeeId = E.Id
INNER JOIN dbo.Projects P ON [PE].ProjectId = P.Id