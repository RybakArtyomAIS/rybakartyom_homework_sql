var map_hero_class = "hero";
var map_trap_class = "trap";
var map_fight_class = "fight";
var map = [];
var currentMapHeroI = 0;
var currentMapHeroJ = 0;
createMap();
updateMap();

function getUserKey(e) {
    if (e.key.toLowerCase() === "w") {
        if (currentMapHeroI - 1 >= 0) {
            map[currentMapHeroI][currentMapHeroJ].isHero = false;
            currentMapHeroI--;
            map[currentMapHeroI][currentMapHeroJ].isHero = true;
            updateMap();
        }
    }
    if (e.key.toLowerCase() === "s") {
        if (currentMapHeroI + 1 <= 3) {
            map[currentMapHeroI][currentMapHeroJ].isHero = false;
            currentMapHeroI++;
            map[currentMapHeroI][currentMapHeroJ].isHero = true;
            updateMap();
        }
    }
    if (e.key.toLowerCase() === "a") {
        if (currentMapHeroJ - 1 >= 0) {
            map[currentMapHeroI][currentMapHeroJ].isHero = false;
            currentMapHeroJ--;
            map[currentMapHeroI][currentMapHeroJ].isHero = true;
            updateMap();
        }
    }
    if (e.key.toLowerCase() === "d") {
        if (currentMapHeroJ + 1 <= 3) {
            map[currentMapHeroI][currentMapHeroJ].isHero = false;
            currentMapHeroJ++;
            map[currentMapHeroI][currentMapHeroJ].isHero = true;
            updateMap();
        }
    }
}

function createMap() {
    document.onkeydown = getUserKey;
    for (var i = 0; i < 4; i++) {
        map[i] = [];
        for (var j = 0; j < 4; j++) {
            map[i][j] = {
                isTrap: false,
                isFight: false,
                isHero: false
            }
        }
    }
    map[0][2].isTrap = true;
    map[2][1].isFight = true;
    map[1][0].isHero = true;
    currentMapHeroI = 1;
    currentMapHeroJ = 0;
}

function updateMap() {
    for (var i = 0; i < 4; i++) {
        for (var j = 0; j < 4; j++) {
            var map_div_id = "map_" + i + "_" + j;
            var mapDiv = document.getElementById(map_div_id);
            mapDiv.innerHTML = "";
            mapDiv.className = "";
            if (map[i][j].isTrap) {
                mapDiv.className = map_trap_class;
            }
            if (map[i][j].isFight) {
                mapDiv.className = map_fight_class;
            }
            if (map[i][j].isHero) {
                mapDiv.className = map_hero_class;
            }
        }
    }
}