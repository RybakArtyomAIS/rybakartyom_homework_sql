var heroSrc = "Room/room_hero.png";
var heroAttackSrc = "Room/room_hero_attack.png";
var heroMagicSrc = "Room/room_hero_magic.png";
var heroDefenseSrc = "Room/room_hero_defendes.png";
var enemySrc = "Room/room_skeleton.png";
var enemyAttackSrc = "Room/room_skeleton_attack.png";

var currentHPHero = 100;
var currentHPEnemy = 100;

function heroAttack() {
    var attackResult = Math.round((20 - 1) * Math.random() + 1);
    currentHPEnemy -= attackResult;
    document.getElementById("roomHero").src = heroAttackSrc;
    showAttackResult(attackResult);
    document.getElementById("roomEnemyHPValue").innerHTML = currentHPEnemy;
    document.getElementById("roomHero").src = heroSrc;
    enemyAttack(0);
}

function heroMagic() {
    var attackResult = Math.round((15 - 5) * Math.random() + 5);
    currentHPEnemy -= attackResult;
    document.getElementById("roomHero").src = heroMagicSrc;
    showAttackResult(attackResult);
    document.getElementById("roomEnemyHPValue").innerHTML = currentHPEnemy;
    document.getElementById("roomHero").src = heroSrc;
    enemyAttack(0);
}

function heroDefense() {
    document.getElementById("roomHero").src = heroDefenseSrc;
    enemyAttack(5);
    document.getElementById("roomHero").src = heroSrc;
}

function enemyAttack(defense) {
    var attackResult = Math.round((25 - 1) * Math.random() + 1 - defense);
    if (attackResult < 0) {
        attackResult = 0;
    }
    document.getElementById("roomEnemy").src = enemyAttackSrc;
    showAttackResult(attackResult);
    currentHPHero -= attackResult;
    document.getElementById("roomEnemy").src = enemySrc;
    document.getElementById("roomHeroHPValue").innerHTML = currentHPHero;
}

function showAttackResult(result) {
    document.getElementById("roomAttackResultValue").innerHTML = result;
    var start = Date.now();

    var timer = setInterval(function() {
        var timePassed = Date.now() - start;

        document.getElementById("roomAttackResult").style.top = Math.round(300 - timePassed / 10) + 'px';
        if (timePassed >= 1000) {
            clearInterval(timer);
            document.getElementById("roomAttackResultValue").innerHTML = "";
            return;
        }

    }, 20);
}