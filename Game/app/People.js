function People(name) {
    this.name = name;
    this.hitPoint = 100;
    this.minAttackValue = 1;
    this.maxAttackValue = 20;
    this.minMagicAttackValue = 5;
    this.maxMagicAttackValue = 15;
    this.armor = 2;
    this.bonusDefense = new BonusDefense(0, 0);
    this.Attack = function() {
        return Math.round(Math.random() * (this.maxAttackValue - this.minAttackValue) + this.minAttackValue);
    }
    this.MagicAttack = function() {
        return Math.round(Math.random() * (this.maxMagicAttackValue - this.minMagicAttackValue) + this.minMagicAttackValue);
    }
    this.SetDefense = function() {
        var armorValue = Math.round(Math.random() * 7);
        this.bonusDefense = new BonusDefense(armorValue, 2);
        return armorValue;
    }
    this.GetAttack = function(attackValue) {
        var hitPointValue = attackValue - this.armor - this.bonusDefense.armor;
        if (hitPointValue < 0) {
            hitPointValue = 0;
        }
        this.hitPoint -= hitPointValue;
        return hitPointValue;
    }
    this.EndRound = function() {
        if (this.bonusDefense.duration > 0) {
            this.bonusDefense.duration--;
        }
        if (this.bonusDefense.duration <= 0) {
            this.bonusDefense.armor = 0;
            this.bonusDefense.duration = 0;
        }
    }
}

function BonusDefense(armor, duration) {
    this.armor = armor;
    this.duration = duration;
}

function CreateEnemies() {
    this.CreateSkeleton = function() {
        var enemy = new People("Skeleton");
        enemy.minAttackValue = 1;
        enemy.maxAttackValue = 20;
        enemy.minMagicAttackValue = 0;
        enemy.maxMagicAttackValue = 0;
        enemy.armor = 0;
        return enemy;
    }
}