function GameRoomManager(hero, enemy) {
    this.hero = hero;
    this.enemy = enemy;
    this.numberRound = 1;
    this.heroAttackValue = 0;
    this.enemyAttackValue = 0;
    this.roomHelper = new GameRoomHelper();
    this.PlayRound = function(userChoise) {
        var resultAction = 0;
        if (userChoise.isAttack) {
            resultAction = this.enemy.GetAttack(hero.Attack());
            this.roomHelper.ShowHeroAttack(resultAction);
        }
        if (userChoise.isMagic) {
            resultAction = this.enemy.GetAttack(hero.MagicAttack());
            this.roomHelper.ShowHeroMagic(resultAction);
        }
        if (userChoise.isDefense) {
            resultAction = hero.SetDefense();
            this.roomHelper.ShowHeroDefense(resultAction);
        }
        if (enemy.hitPoint > 0) {
            resultAction = this.hero.GetAttack(enemy.Attack());
            this.roomHelper.ShowEnemyAttack(resultAction);
            this.numberRound++;
            this.roomHelper.ShowNumberRound(this.numberRound);
        } else {
            this.roomHelper.ShowNumberRound("Win!");
        }
    }
}

function GameRoomHelper() {
    var attackClassName = "attack";
    var magicClassName = "magic";
    var defenseClassName = "defense";
    var heroImageSrc = "Room/room_hero.png";
    var heroAttackImageSrc = "Room/room_hero_attack.png";
    var heroMagicImageSrc = "Room/room_hero_magic.png";
    var heroDefenseImageSrc = "Room/room_hero_defendes.png";
    var enemyImageSrc = "Room/room_skeleton.png";
    var enemyAttackImageSrc = "Room/room_skeleton_attack.png";
    var heroElement = document.getElementById("roomHero");
    var enemyElement = document.getElementById("roomEnemy");
    var heroHpElement = document.getElementById("roomHeroHPValue");
    var enemyHpElement = document.getElementById("roomEnemyHPValue");
    var actionHeroElement = document.getElementById("roomHeroAttackResult");
    var actionEnemyElement = document.getElementById("roomEnemyAttackResult");
    var actionHeroValueElement = document.getElementById("roomHeroAttackResultValue");
    var actionEnemyValueElement = document.getElementById("roomEnemyAttackResultValue");
    var roundElementValue = document.getElementById("roomNumberRoundValue");

    this.ShowHeroAttack = function(damage) {
        var currentHPEnemy = enemyHpElement.innerHTML - damage;
        heroElement.src = heroAttackImageSrc;
        showActionResult(damage, attackClassName);
        enemyHpElement.innerHTML = currentHPEnemy;
    }

    this.ShowHeroMagic = function(damage) {
        var currentHPEnemy = enemyHpElement.innerHTML - damage;
        heroElement.src = heroMagicImageSrc;
        showActionResult(damage, magicClassName);
        enemyHpElement.innerHTML = currentHPEnemy;
    }

    this.ShowHeroDefense = function(defense) {
        heroElement.src = heroDefenseImageSrc;
        showActionResult(defense, defenseClassName);
    }

    this.ShowEnemyAttack = function(damage) {
        var currentHP = heroHpElement.innerHTML - damage;
        enemyElement.src = enemyAttackImageSrc;
        showActionResult(damage, attackClassName, true);
        heroHpElement.innerHTML = currentHP;
    }

    this.ShowNumberRound = function(round) {
        roundElementValue.innerHTML = round;
    }

    function showActionResult(result, kindOfActionClassName, isEnemy) {
        isEnemy = isEnemy || false;
        var actionElement = isEnemy ? actionEnemyElement : actionHeroElement;
        var actionValueElement = isEnemy ? actionEnemyValueElement : actionHeroValueElement;
        actionValueElement.innerHTML = result;
        actionValueElement.className = kindOfActionClassName;
        var start = Date.now();

        var timer = setInterval(function() {
            var timePassed = Date.now() - start;
            actionElement.style.top = Math.round(300 - timePassed / 10) + 'px';
            if (timePassed >= 1000) {
                clearInterval(timer);
                actionValueElement.innerHTML = "";
                if (isEnemy) {
                    enemyElement.src = enemyImageSrc;
                } else {
                    heroElement.src = heroImageSrc;
                }
                return;
            }
        }, 20);
    }
}

function KindOfAction() {
    this.isAttack = false;
    this.isMagic = false;
    this.isDefense = false;
}