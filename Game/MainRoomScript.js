var roomManager = new GameRoomManager(new People("Crusader"), new CreateEnemies().CreateSkeleton());

document.getElementById("roomAttack").onclick = function() {
    var userChoise = new KindOfAction();
    userChoise.isAttack = true;
    roomManager.PlayRound(userChoise);
    return false;
};

document.getElementById("roomMagic").onclick = function() {
    var userChoise = new KindOfAction();
    userChoise.isMagic = true;
    roomManager.PlayRound(userChoise);
    return false;
};

document.getElementById("roomDefense").onclick = function() {
    var userChoise = new KindOfAction();
    userChoise.isDefense = true;
    roomManager.PlayRound(userChoise);
    return false;
};